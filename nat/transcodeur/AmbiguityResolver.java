/*
 * Nat Braille
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * http://natbraille.free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.transcodeur;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import nat.ConfigNat;
import nat.Nat;
import gestionnaires.GestionnaireErreur;

/**
 * Résoud les ambiguïtés détectée lors de la transcription
 * @author bruno
 *
 */
public class AmbiguityResolver
{
	/** Nom du fichier transcrit contenant des ambiguïtés */
	private String fTransAmb ="";
	/** Instance du gestionnaire d'erreurs */
	private GestionnaireErreur gest = null;
	/** liste des ambiguités */
	private ArrayList<Ambiguity> ambiguities = new ArrayList<Ambiguity>();
	
	/**
	 * Crée une instance de AmbiguityResolver
	 * @param fTrAmb adresse du fichier à analyser
	 * @param g instance du gestionnaire d'erreur
	 */
	public AmbiguityResolver(String fTrAmb, GestionnaireErreur g)
	{
		fTransAmb = fTrAmb;
		gest = g;
		resolveAmbiguity();
	}
	
	/**
	 * Charge les ambiguités
	 * @return true si analyse terminée correctement
	 */
	private boolean resolveAmbiguity()
	{
		boolean retour = true;
		
		gest.afficheMessage("Chargement du fichier transcrit pour analyse", Nat.LOG_VERBEUX);
		Document doc;
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(ConfigNat.getCurrentConfig().getNiveauLog()==Nat.LOG_DEBUG);
    
        DocumentBuilder db;
		try
		{
			db = dbf.newDocumentBuilder();
			doc = db.parse(new File(fTransAmb));
			
			NodeList ambs = doc.getElementsByTagName("ambiguity");
			for(int i=0;i<ambs.getLength();i++)
			{	
				ambiguities.add(new Ambiguity(ambs.item(i)));
			}
		}
		catch (ParserConfigurationException e) {e.printStackTrace();}//TODO
		catch (SAXException e) {e.printStackTrace();}//TODO
		catch (IOException e) {e.printStackTrace();}//TODO
		return retour;		
	}
}
