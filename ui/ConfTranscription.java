/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

import nat.ConfigNat;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Onglet de configuration de la transcription
 * @author bruno
 *
 */
public class ConfTranscription extends OngletConf implements ItemListener, ActionListener
{
	/** Pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	/** Bouton activant toute les règles complémentaires de transcription */
	private JButton jbtTout = new JButton("Tout activer (norme)");
	/** Bouton désactivant toutes les règles complémentaires de transcription */
	private JButton jbtRien = new JButton("Ne rien activer");
	/** case à cocher utiliser le double préfixe majuscule*/
	private JCheckBox jchbMajDouble = new JCheckBox("Utiliser le double préfixe pour les mots entièrement en majuscule");
	/** case à cocher utiliser les règles de passage en majuscule*/
	private JCheckBox jchbMajPassage = new JCheckBox("Prendre en compte les passages en majuscules");
	/** case à cocher utiliser les règles de mélange de minuscules et de majuscules*/
	private JCheckBox jchbMajMots = new JCheckBox("Prendre en compte les mélanges de minuscules et majuscules dans un mot");
	/** case à cocher signaler la mise en évidence d'un mot*/
	private JCheckBox jchbEviMot = new JCheckBox("Signaler la mise en évidence d'un mot");
	/** case à cocher utiliser les règles des passages en évidence*/
	private JCheckBox jchbEviPassage = new JCheckBox("Prendre en compte les passages mis en évidence");
	/** case à cocher utiliser les règles des mises en évidence à l'intérieur des mots*/
	private JCheckBox jchbEviDansMot = new JCheckBox("Prendre en compte les mises en évidence à l'interieur d'un mot");
	/** case à cocher utiliser la notation trigonométrique spécifique*/
	private JCheckBox jchbMathsTrigoSpec = new JCheckBox("Utiliser la notation trigonométrique spécifique");
	/** case à cocher utiliser la notation trigonométrique spécifique*/
	private JCheckBox jchbMathsPref = new JCheckBox("Préfixer tous les contenus mathématiques (hors norme)");
	/** case à cocher transcrire les images en braille*/
	private JCheckBox jchbImages = new JCheckBox("Transcrire les images");
	/** JtextField contenant l'adresse du répertoire d'installation d'image magick*/
	private JTextField jtfIM = new JTextField(20);
	/** Bouton ouvrant le JFileChooser permettant de choisir le répertoire d'installation d'image magick */
	JButton jbtChoixIM = new JButton("parcourir...");
	/** jspinner indiquant le niveau de titre à partir duquel on abrège les titres */
	private JSpinner jspAbrTitres = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
	/** JtextField contenant l'adresse du répertoire d'installation d'image magick*/
	
	/** Constructeur */
	public ConfTranscription()
	{
		super();
		getAccessibleContext().setAccessibleDescription("Activez cet onglet pour afficher les options de transcription");
		getAccessibleContext().setAccessibleName("Onglet contenant les options de transcription");
		setToolTipText("Options de transcription");
		/**********
		 * Préparation des composants
		 */
	    
		jbtTout.getAccessibleContext().setAccessibleName("Utiliser toutes les règles complémentaires de la norme");
		//jbtTout.getAccessibleContext().setAccessibleDescription("Validez si vous souhaitez activer toutes les règles complémentaires de la norme");
		jbtTout.setToolTipText("Validez si vous souhaitez activer toutes les règles complémentaires de la norme (Alt+t)");
		jbtTout.setMnemonic('t');
		jbtTout.addActionListener(this);
		
		jbtRien.getAccessibleContext().setAccessibleName("N'utiliser aucune des règles complémentaires de la norme");
		//jbtRien.getAccessibleContext().setAccessibleDescription("Validez si vous souhaitez désactiver toutes les règles complémentaires de la norme");
		jbtRien.setToolTipText("Validez si vous souhaitez désactiver toutes les règles complémentaires de la norme (Alt+r)");
		jbtRien.setMnemonic('r');
		jbtRien.addActionListener(this);
		
		jchbMajDouble.getAccessibleContext().setAccessibleName("Utiliser le double préfixe majuscule pour les mots entièrement en majuscule");
		//jchbMajDouble.getAccessibleContext().setAccessibleDescription("Activez si vous souhaitez activer le double préfixe majuscule pour les mots entièrement en majuscule");
		jchbMajDouble.setToolTipText("Activez si vous souhaitez activer le double préfixe majuscule pour les mots entièrement en majuscule (Alt+d)");
		jchbMajDouble.setMnemonic('d');
		jchbMajDouble.setSelected(ConfigNat.getCurrentConfig().getLitMajDouble());  
		jchbMajDouble.addItemListener(this);
	  
		jchbMajPassage.getAccessibleContext().setAccessibleName("Préfixer les passages en majuscule");
		//jchbMajPassage.getAccessibleContext().setAccessibleDescription("Activez si vous souhaitez activer le préfixage des passages en majuscule");
		jchbMajPassage.setToolTipText("Activez si vous souhaitez activer le préfixage des passages en majuscule (Alt+p)");
		jchbMajPassage.setMnemonic('p');
		jchbMajPassage.setSelected(ConfigNat.getCurrentConfig().getLitMajPassage());    
		jchbMajPassage.addItemListener(this);
		
		jchbMajMots.getAccessibleContext().setAccessibleName("Prendre en compte les mélanges de minuscules et majuscules dans un mot");
		//jchbMajMots.getAccessibleContext().setAccessibleDescription("Activez si vous souhaitez prendre en compte les mélanges de minuscules et majuscules dans un mot");
		jchbMajMots.setToolTipText("Activez si vous souhaitez prendre en compte les mélanges de minuscules et majuscules dans un mot (Alt+m)");
		jchbMajMots.setMnemonic('m');
		jchbMajMots.setSelected(ConfigNat.getCurrentConfig().getLitMajMelange());	    
		jchbMajMots.addItemListener(this);
		
		jchbEviMot.getAccessibleContext().setAccessibleName("Prendre en compte les mots mis en évidence");
		//jchbEviMot.getAccessibleContext().setAccessibleDescription("Activez si vous souhaitez prendre en compte les mots mis en évidence");
		jchbEviMot.setToolTipText("Activez si vous souhaitez prendre en compte les mots mis en évidence (Alt+v)");
		jchbEviMot.setMnemonic('v');
		jchbEviMot.setSelected(ConfigNat.getCurrentConfig().getLitEvidenceMot());	    
		jchbEviMot.addItemListener(this);
		
		jchbEviPassage.getAccessibleContext().setAccessibleName("Prendre en compte les passages en évidence");
		//jchbEviPassage.getAccessibleContext().setAccessibleDescription("Activez si vous souhaitez prendre en compte les passages en évidence");
		jchbEviPassage.setToolTipText("Activez si vous souhaitez prendre en compte les passages en évidence (Alt+e)");
		jchbEviPassage.setMnemonic('e');
		jchbEviPassage.setSelected(ConfigNat.getCurrentConfig().getLitEvidencePassage());	    
		jchbEviPassage.addItemListener(this);
		
		jchbEviDansMot.getAccessibleContext().setAccessibleName("Prendre en compte les mises en évidence à l'intérieur d'un mot");
		//jchbEviDansMot.getAccessibleContext().setAccessibleDescription("Activez si vous souhaitez prendre en compte les mises en évidence à l'interieur d'un mot");
		jchbEviDansMot.setToolTipText("Activez si vous souhaitez prendre en compte les mises en évidence à l'intérieur d'un mot (Alt+i)");
		jchbEviDansMot.setMnemonic('i');
		jchbEviDansMot.setSelected(ConfigNat.getCurrentConfig().getLitEvidenceDansMot());	    
		jchbEviDansMot.addItemListener(this);
		
		jchbMathsTrigoSpec.setSelected(ConfigNat.getCurrentConfig().getMathTrigoSpec());
		jchbMathsTrigoSpec.getAccessibleContext().setAccessibleName("Notation spécifique trigo");
		//jchbMathsTrigoSpec.getAccessibleContext().setAccessibleDescription("Cocher cette case pour activer la notation spécifique trigo");
		jchbMathsTrigoSpec.setToolTipText("Activer la notation spécifique trigo (Alt+q)");
		jchbMathsTrigoSpec.setMnemonic('q');
		jchbMathsTrigoSpec.addItemListener(this);
		
		jchbMathsPref.setSelected(ConfigNat.getCurrentConfig().getMathPrefixAlways());
		//jchbMathsPref.getAccessibleContext().setAccessibleName("Utilisation systématique du préfixe mathématique");
		//jchbMathsPref.getAccessibleContext().setAccessibleDescription("Activer pour préfixer un contenu mathématique avec pt6 pt3 (ne respecte pas la norme)");
		jchbMathsPref.setToolTipText("Toujours préfixer un contenu mathématique avec pt6 pt3, ne respecte pas la norme (Alt+x)");
		jchbMathsPref.setMnemonic('x');
		jchbMathsPref.addItemListener(this);
		
		jchbImages.setSelected(ConfigNat.getCurrentConfig().getTranscrireImages());
		//jchbImages.getAccessibleContext().setAccessibleName("Transcription des images");
		//jchbImages.getAccessibleContext().setAccessibleDescription("Activer la transcription des images");
		jchbImages.setToolTipText("Transcrire les images en braille (peut donner n'importe quoi...) (Alt+a)");
		jchbImages.setMnemonic('a');
		
		jtfIM.setText(ConfigNat.getCurrentConfig().getImageMagickDir());
		jtfIM.getAccessibleContext().setAccessibleDescription("Entrez l'adresse de l'application externe d'édition");
		jtfIM.getAccessibleContext().setAccessibleName("Zone de texte adresse de l'application externe");
		jtfIM.setToolTipText("Adresse de l'application externe (Alt+x)");
		
		jbtChoixIM.addActionListener(this);
		jbtChoixIM.getAccessibleContext().setAccessibleName("Emplacement d'Image Magick");
		//jbtChoixIM.getAccessibleContext().setAccessibleDescription("Valider pour rechercher le répertoire d'installation d'image magick");
		jbtChoixIM.setToolTipText("Recherche du répertoire d'installation d'image magick (Alt+c)");
		jbtChoixIM.setMnemonic('c');
		
		JLabel lJspAbrTitres = new JLabel("Abréger les titres à partir du niveau :");
		lJspAbrTitres.setLabelFor(jspAbrTitres);
		jspAbrTitres.setValue(ConfigNat.getCurrentConfig().getNiveauTitreAbrege());
		jspAbrTitres.getAccessibleContext().setAccessibleName("Niveau de titre minimal pour abréger");
		jspAbrTitres.setToolTipText("Les titres d'un niveau strictement inférieur dans le document d'origine ne seront pas abrégés (alt+b)");
		jspAbrTitres.getAccessibleContext().setAccessibleDescription(getToolTipText());
		lJspAbrTitres.setDisplayedMnemonic('b');
		/*********
		 * Mise en page
		 */
		
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		
		
		JLabel titre = new JLabel("<html><h3>Paramétrage de la transcription</h3></html>");
		JLabel titre2 = new JLabel("<html><h4>Intégral</h4></html>");
		JLabel titre2_1 = new JLabel("<html><h4>Abrégé</h4></html>");
		JLabel titre3 = new JLabel("<html><h4>Braille mathématique</h4></html>");
		JLabel titre4 = new JLabel("<html><h4>Images et graphiques (hors norme)</h4></html>");
		
		JLabel lIM = new JLabel("Répertoire d'installation d'ImageMagick:");
		lIM.setLabelFor(jtfIM);
		lIM.setDisplayedMnemonic('k');
		
		JPanel panTr = new JPanel();
		panTr.setLayout(gbl);
		gbc.anchor = GridBagConstraints.WEST;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth=3;
		gbl.setConstraints(titre, gbc);
		panTr.add(titre);
		
		gbc.gridy++;
		gbc.gridx = 1;
		gbc.gridwidth=1;
		gbl.setConstraints(jbtTout, gbc);
		panTr.add(jbtTout);
		
		gbc.gridx=3;
		gbc.gridwidth=1;
		gbl.setConstraints(jbtRien, gbc);
		panTr.add(jbtRien);
		
		gbc.gridx=0;
		gbc.gridy++;
		gbc.gridwidth=1;
		gbl.setConstraints(titre2, gbc);
		panTr.add(titre2);
		
		gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth=2;
		gbl.setConstraints(jchbMajDouble, gbc);
		panTr.add(jchbMajDouble);
		
		gbc.gridy++;
		gbl.setConstraints(jchbMajPassage, gbc);
		panTr.add(jchbMajPassage);
		
		gbc.gridy++;
		gbl.setConstraints(jchbMajMots, gbc);
		panTr.add(jchbMajMots);
		
		gbc.gridy++;
		gbl.setConstraints(jchbEviMot, gbc);
		panTr.add(jchbEviMot);
		
		gbc.gridy++;
		gbl.setConstraints(jchbEviDansMot, gbc);
		panTr.add(jchbEviDansMot);
		
		gbc.gridy++;
		gbl.setConstraints(jchbEviPassage, gbc);
		panTr.add(jchbEviPassage);
		
		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbl.setConstraints(titre2_1, gbc);
		panTr.add(titre2_1);
		
		gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth = 2;
		gbl.setConstraints(lJspAbrTitres, gbc);
		panTr.add(lJspAbrTitres);
		
		gbc.gridx++;
		gbc.gridwidth = 1;
		gbl.setConstraints(jspAbrTitres, gbc);
		panTr.add(jspAbrTitres);
		
		gbc.gridy++;
		gbc.gridx = 0;
		gbc.gridwidth = 3;
		gbl.setConstraints(titre3, gbc);
		panTr.add(titre3);
		
		gbc.gridy++;
		gbc.gridx++;
		gbc.gridwidth = 2;
		gbl.setConstraints(jchbMathsTrigoSpec, gbc);
		panTr.add(jchbMathsTrigoSpec);
		
		gbc.gridy++;
		panTr.add(jchbMathsPref,gbc);
		
		gbc.gridy++;
		gbc.gridx = 0;
		gbl.setConstraints(titre4, gbc);
		panTr.add(titre4);
		
		gbc.gridy++;
		gbc.gridx++;
		gbl.setConstraints(jchbImages, gbc);
		panTr.add(jchbImages);
		
		gbc.gridy++;
		gbc.gridwidth = 1;
		gbl.setConstraints(lIM, gbc);
		panTr.add(lIM);

		gbc.gridx++;
		gbl.setConstraints(jtfIM, gbc);
		panTr.add(jtfIM);
		
		gbc.gridx++;
		gbl.setConstraints(jbtChoixIM, gbc);
		panTr.add(jbtChoixIM);

		add(panTr);
	}	
	/**
	 * Implémentation de java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 * <p>Active (resp. désactive) les boutons appliquant toutes (resp. aucune) règles</p>
	 * @see #jbtRien
	 * @see #jbtTout
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	public void itemStateChanged(ItemEvent ie) 
	{
		jbtTout.setEnabled(true);
		jbtRien.setEnabled(true);
		if(((JCheckBox)(ie.getSource())).isSelected())
		{
			if(jchbMajDouble.isSelected()&&jchbMajPassage.isSelected()&&jchbMajMots.isSelected()&&
					jchbEviMot.isSelected()&&jchbEviPassage.isSelected()&&jchbEviDansMot.isSelected()
					&&jchbMathsTrigoSpec.isSelected())
			{
				jbtTout.setEnabled(false);
			}
		}
		else if(!(jchbMajDouble.isSelected()||jchbMajPassage.isSelected()||jchbMajMots.isSelected()||
					jchbEviMot.isSelected()||jchbEviPassage.isSelected()||jchbEviDansMot.isSelected()
					||jchbMathsTrigoSpec.isSelected()))
		{
			jbtRien.setEnabled(false);
		}		
	}
	/**
	 * Implémentation de java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 * Active (resp. désactive) toutes (resp.aucune) règle(s)
	 * @see #jbtRien
	 * @see #jbtTout
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent ae) 
	{
		if(ae.getSource()==jbtTout)
		{
			jchbMajDouble.setSelected(true);
			jchbMajPassage.setSelected(true);
			jchbMajMots.setSelected(true);
			jchbEviMot.setSelected(true);
			jchbEviPassage.setSelected(true);
			jchbEviDansMot.setSelected(true);
			jchbMathsTrigoSpec.setSelected(true);
			jbtTout.setEnabled(false);
			jbtRien.setEnabled(true);
		}
		else if(ae.getSource()==jbtRien)
		{
			jchbMajDouble.setSelected(false);
			jchbMajPassage.setSelected(false);
			jchbMajMots.setSelected(false);
			jchbEviMot.setSelected(false);
			jchbEviPassage.setSelected(false);
			jchbEviDansMot.setSelected(false);
			jchbMathsTrigoSpec.setSelected(false);
			jbtRien.setEnabled(false);
			jbtTout.setEnabled(true);
		}
		else if (ae.getSource()==jbtChoixIM)
		{
			JFileChooser selectionneAppli = new JFileChooser();
			selectionneAppli.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			selectionneAppli.setDialogTitle("Sélection du répertoire d'installation d'image magick");
	 		if (selectionneAppli.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
	         {    
	 			//si un fichier est selectionné, récupérer le fichier puis son path
	 			jtfIM.setText(selectionneAppli.getSelectedFile().getAbsolutePath()); 
	         }
		}
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer()
	 */
	public boolean enregistrer()
	{
		boolean retour = true;
		ConfigNat.getCurrentConfig().setLitEvidenceMot(jchbEviMot.isSelected());
		ConfigNat.getCurrentConfig().setLitEvidencePassage(jchbEviPassage.isSelected());
		ConfigNat.getCurrentConfig().setLitEvidenceDansMot(jchbEviDansMot.isSelected());
		ConfigNat.getCurrentConfig().setLitMajDouble(jchbMajDouble.isSelected());
		ConfigNat.getCurrentConfig().setLitMajMelange(jchbMajMots.isSelected());
		ConfigNat.getCurrentConfig().setLitMajPassage(jchbMajPassage.isSelected());
		ConfigNat.getCurrentConfig().setMathTrigoSpec(jchbMathsTrigoSpec.isSelected());
		ConfigNat.getCurrentConfig().setMathPrefixAlways(jchbMathsPref.isSelected());
		ConfigNat.getCurrentConfig().setTranscrireImages(jchbImages.isSelected());
		ConfigNat.getCurrentConfig().setImageMagickDir(jtfIM.getText());
		ConfigNat.getCurrentConfig().setNiveauTitreAbrege(((Integer)(jspAbrTitres.getValue())).intValue());
		
		ConfigNat.getCurrentConfig().sauvegarder();
		return retour;
	}
}
