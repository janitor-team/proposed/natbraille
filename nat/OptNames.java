/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat;

/**
 * Classe pour le nom des options avec JOptSimple
 * <p>Contient uniquement des attributs public et static 
 * pour le nom des options en ligne de commande</p>
 * @author Vivien et Bruno
 *
 */
public class OptNames
{
    // general
	/** constante valant "ge-log-level"*/
    public static final String ge_log_verbosity = "ge-log-level";
    /** constante valant "conf-version" */
    public static final String conf_version = "conf-version" ;
    /** constante valant "ge-check-update" */
    public static final String ge_check_update = "ge-check-update";

    // ui - editor
	/** constante valant "ui-editor-braille-font"*/
    public static final String ui_editor_font = "ui-editor-braille-font";
	/** constante valant "ui-editor-braille-font-size"*/
    public static final String ui_editor_font_size = "ui-editor-braille-font-size";
	/** constante valant "ui-editor-display-zone2"*/
    public static final String ui_editor_zone2_display = "ui-editor-display-zone2";
	/** constante valant "ui-editor-font"*/
    public static final String ui_editor_font2 = "ui-editor-font";
	/** constante valant "ui-editor-font-size"*/
    public static final String ui_editor_font2_size = "ui-editor-font-size";
	/** constante valant "ui-editor-auto-open"*/
    public static final String ui_editor_auto_open = "ui-editor-auto-open";
	/** constante valant "ui-editor-external*/
    public static final String ui_editor_external = "ui-editor-external";
	/** constante valant "ui-editor-nat"*/
    public static final String ui_editor_nat = "ui-editor-nat";
	/** constante valant "ui-editor-default"*/
    public static final String ui_editor_default = "ui-editor-default";
	/** constante valant "ui-remember-windows-size"*/
	public static final String ui_remember_windows_size = "ui-remember-windows-size";
	/** constante valant "ui-center-windows"*/
	public static final String ui_center_windows = "ui-center-windows";
    
    // todo : get et set dans confignat et relier à l'utilisation (pour l'instant : constant"
	/** constante valant "ui-filter-conf-dir"*/
    public static final String ui_filter_conf_dir = "ui-filter-conf-dir";

    // last in and out document file
	/** constante valant "last-source-filename"*/
    public static final String last_source_filename = "last-source-filename";
	/** constante valant "last-source-encoding"*/
    public static final String last_source_encoding = "last-source-encoding";
	/** constante valant "last-dest-filename"*/
    public static final String last_dest_filename = "last-dest-filename";
	/** constante valant "last-dest-encoding"*/
    public static final String last_dest_encoding = "last-dest-encoding";
	/** constante valant "last-filter-configuration-filename"*/
    public static final String last_filter_configuration_filename = "last-filter-configuration-filename";

    // filters
	/** constante valant "fi-is-sys-config"*/
	public static final String fi_is_sys_config = "fi-is-sys-config";
	/** constante valant "fi-name"*/
	public static final String fi_name="fi-name";
	/** constante valant "fi-infos"*/
	public static final String fi_infos="fi-infos";
	/** constante valant "fi-optimize"*/
	public static final String fi_optimize = "fi-optimize";

	/** constante valant "fi-dtd-filename"*/
    public static final String fi_dtd_filename ="fi-dtd-filename";
	/** constante valant "fi-braille-table"*/
    public static final String fi_braille_table = "fi-braille-table";
    /** constante valant "fi-is-sys-braille-table*/
    public static final String fi_is_sys_braille_table = "fi-is-sys-braille-table";
    /** constante valant "fi-is-sys-emboss-table*/
    public static final String fi_is_sys_emboss_table = "fi-is-sys-emboss-table";
	/** constante valant "fi-filter-filename"*/
    public static final String fi_filter_filename = "fi-filter-filename"; // xsl
	/** constante valant "fi-tag-doc-begin"*/
    public static final String fi_tag_doc_begin = "fi-tag-doc-begin";
	/** constante valant "fi-tag-doc-end"*/
    public static final String fi_tag_doc_end = "fi-tag-doc-end";
	/** constante valant "fi-tag-par-begin"*/
    public static final String fi_tag_par_begin = "fi-tag-par-begin";
	/** constante valant "fi-tag-par-end"*/
    public static final String fi_tag_par_end = "fi-tag-par-end";

    // maths filter
	/** constante valant "fi-math-transcribe"*/
    public static final String fi_math_transcribe = "fi-math-transcribe";
	/** constante valant "fi-math-filter-filename"*/
    public static final String fi_math_filter_filename = "fi-math-filter-filename";
	/** constante valant "fi-math-use-trigo-notation"*/
    public static final String fi_math_use_trigo_spec =  "fi-math-use-trigo-notation";
    /** constante valant "fi-math-force-prefix"*/
    public static final String fi_math_force_prefix = "fi-math-force-prefix";
	/** constante valant "fi-math-tag-deb"*/
    public static final String fi_math_tag_deb = "fi-math-tag-deb";
	/** constante valant "fi-math-tag-end"*/
    public static final String fi_math_tag_end = "fi-math-tag-end";

    // litt filter
	/** constante valant "fi-litt-transcribe"*/
    public static final String fi_litt_transcribe = "fi-litt-transcribe";
	/** constante valant "fi-litt-fr-int-filter-filename"*/
    public static final String fi_litt_fr_int_filter_filename = "fi-litt-fr-int-filter-filename";
	/** constante valant "fi-litt-fr-abbreg-filter-filename"*/
    public static final String fi_litt_fr_abbreg_filter_filename = "fi-litt-fr-abbreg-filter-filename";
    /** constante valant "fi-litt-fr-abbreg-rules-filename"*/
	public static final String fi_litt_fr_abbreg_rules_filename = "fi-litt-fr-abbreg-rules-filename";
	/** constante valant "fi-litt-fr-abbreg-rules-filename-perso"*/
	public static final String fi_litt_fr_abbreg_rules_filename_perso = "fi-litt-fr-abbreg-rules-filename-perso";
    /** constante valant "fi-litt-fr-abbreg-rules-filter-filename"*/
	public static final String fi_litt_fr_abbreg_rules_filter_filename = "fi-litt-fr-abbreg-rules-filter-filename";
	/** constante valant "fi-litt-abbreg"*/
    public static final String fi_litt_abbreg = "fi-litt-abbreg";
	/** constante valant "fi-litt-tag-deb"*/
    public static final String fi_litt_tag_deb = "fi-litt-tag-deb";
	/** constante valant "fi-litt-tag-end"*/
    public static final String fi_litt_tag_end = "fi-litt-tag-end";

    // music filter
	/** constante valant "fi-music-transcribe"*/
    public static final String fi_music_transcribe = "fi-music-transcribe";
	/** constante valant "fi-music-filter-filename"*/
    public static final String fi_music_filter_filename = "fi-music-filter-filename";
	/** constante valant "fi-music-tag-deb"*/
    public static final String fi_music_tag_deb = "fi-music-tag-deb";
	/** constante valant "fi-music-tag-end"*/
    public static final String fi_music_tag_end = "fi-music-tag-end";

    // hyphenation
	/** constante valant "fi-hyphenation"*/
    public static final String fi_hyphenation = "fi-hyphenation";
	/** constante valant "fi-hyphenation-lit"*/
    public static final String fi_hyphenation_lit = "fi-hyphenation-lit";
	/** constante valant "fi-hyphenation-dirty"*/
    public static final String fi_hyphenation_dirty = "fi-hyphenation-dirty";
	/** constante valant "fi-hyphenation-rulefile-name"*/
    public static final String fi_hyphenation_rulefile_name = "fi-hyphenation-rulefile-name";
	/** constante valant "fi-hyphenation-rulefile-desc"*/
    public static final String fi_hyphenation_rulefile_desc = "fi-hyphenation-rulefile-desc";
    
    // emboss & print
	/** constante valant "pr-emboss-table"*/
    public static final String pr_emboss_table = "pr-emboss-table";
	/** constante valant "pr-use-emboss-command"*/
    public static final String pr_use_emboss_command = "pr-use-emboss-command";
	/** constante valant "pr-emboss-command"*/
    public static final String pr_emboss_command = "pr-emboss-command";
	/** constante valant "pr-emboss-auto"*/
    public static final String pr_emboss_auto = "pr-emboss-auto";
	/** constante valant "pr-emboss-print-service"*/
    public static final String pr_emboss_print_service = "pr-emboss-print-service";
	/** constante valant "pr-os"*/
    public static final String pr_os = "pr-os";
    
    // encoding
	/** constante valant "in-encoding"*/
    public static final String en_in  = "in-encoding";
	/** constante valant "out-encoding"*/
    public static final String en_out = "out-encoding";

    // transcription
	/** constante valant "tr-litt-use-double-upper-prefix"*/
	public static final String tr_litt_double_upper = "tr-litt-use-double-upper-prefix";
	/** constante valant "tr-litt-use-part-upper-prefix"*/
	public static final String tr_litt_part_upper = "tr-litt-use-part-upper-prefix";
	/** constante valant "tr-litt-use-mixed-upper-lower-rules"*/
	public static final String tr_litt_mixed_upper = "tr-litt-use-mixed-upper-lower-rules";
	/** constante valant "tr-litt-use-word-emphasis-prefix"*/
	public static final String tr_litt_word_emph = "tr-litt-use-word-emphasis-prefix";
	/** constante valant "tr-litt-use-part-emphasis-prefix"*/
	public static final String tr_litt_part_emph ="tr-litt-use-part-emphasis-prefix";
	/** constante valant "tr-litt-show-in-word-emphasis"*/
	public static final String tr_litt_in_word_emph = "tr-litt-show-in-word-emphasis";
	/** constante valant "tr-image-processing"*/
	public static final String tr_image_processing = "tr-image-processing";
	/** constante valant tr-image-magick-dir*/
	public static final String tr_image_magick_dir = "tr-image-magick-dir";
	/** constante valant tr-min-title-contracted */
	public static final String tr_min_title_contracted ="tr-min-title-contracted";
	
	// options avancées
	/** constante valant "tr-use-saxon-processor"*/
	public static final String tr_use_saxon_processor = "tr-use-saxon-processor";
	/** constante valant "ad-nb-log-files"*/
	public static final String ad_nb_log_files = "ad-nb-log-files";
	/** constante valant "ad-log-file-size"*/
	public static final String ad_log_file_size = "ad-log-file-size";
	
	//page format
	/** constante valant "pf-do-layout"*/
	public static final String pf_do_layout = "pf-do-layout";
	/** constante valant "fi-line-length"*/
	public static final String fi_line_lenght = "fi-line-length";
	/** constante valant "fi-line-number"*/
    public static final String fi_line_number = "fi-line-number";
	/** constante valant "pf-empty-line-mode"*/
	public static final String pf_empty_line_mode = "pf-empty-line-mode";
	/** constante valant "pf-min-empty-line-1"*/
	public static final String pf_min_empty_line_1 = "pf-min-empty-line-1";
	/** constante valant "pf-min-empty-line-2"*/
	public static final String pf_min_empty_line_2 = "pf-min-empty-line-2";
	/** constante valant "pf-min-empty-line-3"*/
	public static final String pf_min_empty_line_3 = "pf-min-empty-line-3";
	/** constante valant "pf-min-page-break"*/
	public static final String pf_min_page_break = "pf-min-page-break";
	/** constante valant "pf-generate-page-break"*/
	public static final String pf_generate_page_break = "pf-generate-page-break";
	/** constante valant "pf-add-form-feed"*/
	public static final String pf_add_form_feed = "pf-add-form-feed";
	/** constante valant "pf-strict-titles"*/
	public static final String pf_strict_titles = "pf-strict-titles";
	// public static final String pf_par_indent = "pf-par-indent";
	/** constante valant "pf-linearise-table"*/
	public static final String pf_linearise_table = "pf-linearise-table";
	/** constante valant "pf-min-cell-linearise"*/
	public static final String pf_min_cell_linearise = "pf-min-cell-linearise";
	/** constante valant "pf-numbering-style"*/
	public static final String pf_numbering_style = "pf-numbering-style";
	/** constante valant "pf-number-first-page"*/
	public static final String pf_number_first_page = "pf-number-first-page";
	/** constante valant "pf-titles-levels"*/
	public static final String pf_titles_levels = "pf-titles-levels";
	/** constante valant "pf-strings-addons"*/
	public static final String pf_strings_addons = "pf-strings-addons";
	/** constante valant "pf-strings-addons-count"*/
	public static final String pf_strings_addons_count = "pf-strings-addons-count";
	/** constante valant "pf-strings-addons"*/
	public static final String pf_string_replace_in = "pf-string-replace-in";
	/** constante valant "pf-strings-addons-count"*/
	public static final String pf_string_replace_out = "pf-string-replace-out";
	
	//dimensions des fenetres de l'interface
	/** constante valant "ui-y-editor" : hauteur de l'éditeur*/
	public static final String ui_y_editor = "ui-y-editor";
	/** constante valant "ui-x-editor" : largeur de l'éditeur*/
	public static final String ui_x_editor = "ui-x-editor";
	/** constante valant "ui-max-editor : éditeur maximisé"*/
	public static final String ui_max_editor = "ui-max-editor";
	/** constante valant "ui-y-princ" : hauteur de la fenêtre principale*/
	public static final String ui_y_princ = "ui-y-princ";
	/** constante valant "ui-x-princ" : largeur de la fenêtre principale*/
	public static final String ui_x_princ = "ui-x-princ";
	/** constante valant "ui-max-princ : fenêtre principale maximisée"*/
	public static final String ui_max_princ = "ui-max-princ";
	/** constante valant "ui-y-options" : hauteur de la fenêtre des options*/
	public static final String ui_y_options = "ui-y-options";
	/** constante valant "ui-x-options" : largeur de la fenêtre des options*/
	public static final String ui_x_options = "ui-x-options";
	/** constante valant "ui-max-options : fenêtre des options maximisée"*/
	public static final String ui_max_options = "ui-max-options";
	
	//options sonores
	/** constante valant "ui-sound-during-work*/
	public static final String ui_sound_during_work = "ui-sound-during-work";
	/** constante valant "ui-sound-at-end"*/
	public static final String ui_sound_at_end = "ui-sound-at-end";
	
	//autres options pour l'interfaces
	/** Constante valant "ui-output-file-auto"*/
	public static final String ui_output_file_auto = "ui-output-file-auto";
	/** Constante valant "ui-reverse-trans" */
	public static final String ui_reverse_trans = "ui-reverse-trans";
	
}