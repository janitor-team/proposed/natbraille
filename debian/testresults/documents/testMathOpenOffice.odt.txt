  ¨jeu de test

  ¨types d'expressions mathé-
matiques

  d'¨open¨office prises en
compte par ¨¨nat v`1.3.2


          ¨préfixes

       ¨indicateurs de
      changement de code

  `1/2 ¨ou `1

  et plus simple: ¤a et ¨¤p

  et encore plus simple: m, x,
¨a, `2 et ¨¤d.

`1

1!2"3

x!1"7

  expression:               `2
`'x/„x!2y^6; `2¨a

  deux formules qui se suivent
à la ligne suivante:

  x/„x!2y^6; x/„x!2y^6;

     ¨préfixes majuscule

  ¨en ¨¨litteraire c'est dif-
férent
`¨des¨m¨a¨t¨h¨e¨m¨a¨t¨i¨q¨u¨e´
¨s

2x¨x"¨yy

¨y^2"2y¨x


         ¨opérations


!a;-a;!-a;-!a;´-a;a!b;a**b;a*´
b;a´*b;a¤*b;a-b;a/b;a:b;a/b;a´
¤?b;a¸0b

                            `3
           ¨angles


  `¤:x¨oy, `¤:¨a¨b¨c et voilà
`¤:¨a¨b.


           ¨¨unites


  ¨alors on a `5km"5000m et
`9m^2


          ¨relations


a"b;a¨"b;a´"b;aéb;anonéb;a´
´2b;a´@b;a´"b;a¸8b;a¤8b;

a¤2b;a¤@b;a~b;a""b;a¤2b;a´
¤@b;a¨¨2b;a¸5b;a´:b;a´
´:,b;a:,b;



                            `4
          ¨ensembles


a¤1b;a¤/b;a¨¤1b;a¤!b;a¸!b;´
adifférenceb;a/b;a¨1b;a¸¨1b;´
a´1b;´
a(inclus_au_sens_large(ndnn))´
b;a¨/b;a¸¨/b;a´/b;´
a(ni_un_sur-ensemble_ni_égal_´
à (ndnn))b

¤0;¨¨n;¨¨z;¨¨q;¨¨r;¨¨c;


   ¨parenthèses et crochets

           ¨petits

(a);àaù;¨àa¨ù;éaé;¸8a¸8;´
¨(a¨);´
(chevron_gauche_ndnn)a(chevro´
n_droit_ndnn);´
(chevron_gauche_ndnn)aéb(chev´
ron_droit_ndnn)


                            `5
           ¨grands

(a);àaù;¨àa¨ù;¨(a¨(;a;aéb;a;´
a;éaé;a;b„a;`;a????b;

        ¨imbrications

  à1/(x!à1/(x!1)ù)ù
((1/(x!(1/(x!1))))/2)


          ¨fonctions


lima!b;¨¤sa;¨¤pa;co^pa;ça;´
çça;ççça;¨ça;¨çça;¨ççça;´
ç??a^^„b;c;ç??„a"1;b;açb

e^a;ln(a);exp(a);log(a);a^b;´
@a;^a@b;éaé;a¸*;¨card(a)

ln„a;`;exp„a;`;log„a;`;´
¨card„a;



                            `6
        ¨trigonométrie

ó(a);¨ó(a);è(a);¨è(a);¨è(a);´
¤ó(a);¤¨ó(a);¤è(a);¤¨è(a);´
¤¨è(a);sec(a);csc(a);

óa;¨óa;èa;¨èa;¨èa;¤óa;¤¨óa;´
¤èa;¤¨èa;¤¨èa;sec„a;`;csc„a;`;

sinh(a);sh(a);cosh(a);ch(a);´
tanh(a);coth(a);arcsinh(a);´
arccosh(a);arctanh(a);´
arccoth(a);sech(a);csch(a);

sinh„a;`;sh„a;`;cosh„a;`;´
ch„a;`;tanh„a;`;coth„a;`;´
arcsinh„a;`;arccosh„a;`;´
arctanh„a;`;arccoth„a;`;´
sech„a;`;csch„a;`;

ó(a!b)"¨óa¨ób!óaób

ó„2x;"è¤p¨ó(¤p/2)!è2(¤p/x)

sin^2(x)!cos^2(x)"1

ó70´o!¨ó30´o                `7


        ¨signes divers


¤c;´d;´
¤^d;¸1;¸/;h_bar_ndnn;¤¨r;¤¨i;´
¨´p;¸9;´
¸5;↔;¤7;¨7;...;...;...;...;...

¤*;¤:;¤1;"";¤c;¤/;¨";¤?;´óó;´
^@;´´2;¸5


          ¨attributs


accent_aigua;accent_gravea;´
ˇa;^^:a;petit_ronda;¨:a;¨:ab;´
´¸:a;´¸:ab;¤:a;¤:ab;¸:a;¸:ab;´
¨.a;¨.¨.a;...a;a??¸-;a;a;a;a;





                            `8
        ¨alphabets et
           lettres
        particulières

         ¨lettres de
       l'alphabet latin

a¨ab¨bc¨cd¨de¨ef¨fg¨gh¨hi¨ij¨´
jk¨kl¨lm¨mn¨no¨op¨pq¨qr¨rs¨st´
¨tu¨uv¨vw¨wx¨xy¨yz¨z

      ¨lettres grecques

¤a;¤b;¤g;¤d;¤e;¤z;¤h;¤j;¤i;´
¤k;¤l;¤m;¤n;¤x;¤o;¤p;¤r;¤s;´
¤t;¤u;¤f;¤q;¤y;¤w;

¨a;¨b;¨¤g;¨¤d;¨e;¨z;¨h;¨¤j;´
¨i;¨k;¨¤l;¨m;¨n;¨¤w;¨o;¨¤p;´
¨p;¨¤s;¨t;¨¤u;¨¤f;¨x;¨¤y;¨¤w;

¤e;¤f;¤7;¤r;¤s;¤j;




                            `9
     ¨lettres hébra7ques

¤¤a;¤¤b;¤¤g;¤¤d;¤¤h;¤¤w;¤¤z;´
¤¤x;¤¤t;¤¤j;¤¤k;¤¤1;¤¤l;¤¤m;´
¤¤m;¤¤n;¤¤n;¤¤s;¤¤6;¤¤p;¤¤f;´
¤¤è;¤¤è;¤¤q;¤¤r;¤¤3;¤¤8;

       ¨lettres rondes

¨´b;´e;¨´e;¨´f;¨´h;¨´i;¨´l;´
´l;¨´m;´o;ℛ;


     ¨signes non traités


ƛ


     ¨signes traités mais
     non trouvés dans la
            norme


anonéb;adifférenceb;´
a(inclus_au_sens_large(ndnn))´
b;´                        `10
a(ni_un_sur-ensemble_ni_égal_´
à (ndnn))b;¨àa;b¨ù;´
(chevron_gauche_ndnn)a(chevro´
n_droit_ndnn);´
(chevron_gauche_ndnn)aéb(chev´
ron_droit_ndnn);co^pa;´
h_bar_ndnn;accent_aigub;´
accent_graveb;


      ¨signes non prévus
        par openoffice


a´@´2b;


           ¨erreurs


  `'¨ó„¤p/2; ¨fermeture de
bloc du cos trop t4t: utiliser
des parenthèses

  `'sin^2„x;!cos^2„x;"1 pas de
blocs
  `'ó^2„x;!¨ó^2„x;"1       `11
bloc pour indice et pas pour x

  `ó^2(x)!¨ó^2(x)"1 bloc pour
indice























