/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package ui;

import gestionnaires.GestionnaireErreur;

import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JScrollPane;

import nat.ConfigNat;
import nat.Nat;

import jaxe.JaxeDocument;
import jaxe.JaxeTextPane;
/**
 * Cette classe de l'interface graphique permet d'afficher un fichier à mettre en page (au format nat) et d'intéragir avec lui
 * @author Bruno Mascret
 */
public class EditeurXML extends JFrame
{

	/** Pour la sérialisation (non-utilisé dans Nat)*/
	private static final long serialVersionUID = 1L;
	/** le ScrollPane associé au JTextPane Jaxe */
	private JScrollPane scrollRes;
	/** une instance d'URL du document à représenter */ 
	private URL document;
	/** une instance de GestionnaireErreur */
	private GestionnaireErreur gest;
	/**
	 * Constructeur par défaut. Fabrique une fenêtre basée sur Jaxe
	 * Utilise tmp_mep.xml comme source pour {@link #document}
	 * @param g une instance de {@link GestionnaireErreur}
	 * @see GestionnaireErreur
	 */
	public EditeurXML(GestionnaireErreur g)
	{
		gest = g;
		//File racine = new File("");
		try{document = new URL("file://"+ConfigNat.getUserTempFolder()+"/tmp_mep.xml");}
		catch (MalformedURLException e){gest.afficheMessage("URL mal formée", Nat.LOG_SILENCIEUX);}
		fabriqueEditeur();
	}
	
	/**
	 * Constructeur. Fabrique une fenêtre basée sur Jaxe
	 * @param doc l'adresse du document à représenter
	 * @param g une instance de {@link GestionnaireErreur}
	 * @see GestionnaireErreur
	 */
	public EditeurXML(GestionnaireErreur g, String doc)
	{
		gest = g;
		try{document = new URL("file://"+doc);}
		catch (MalformedURLException e){gest.afficheMessage("URL mal formée", Nat.LOG_SILENCIEUX);}
		fabriqueEditeur();
	}
	
	/**
	 * Fabrique la fenêtre de l'Editeur XML (mise en page)
	 * et le document Jaxe associé
	 */
	private void fabriqueEditeur()
	{
		JaxeDocument jaxeDoc = new JaxeDocument("config/doc_config.xml");
		JaxeTextPane jaxeTextPane = new JaxeTextPane(jaxeDoc, this);
		jaxeDoc.encodage="UTF-8";
		//jaxeDoc.cfg = new Config("config/doc_config.xml",true);
		jaxeDoc.lire(document);
		
//		 or jaxeDoc.setDOMDoc(domDocument);
//		add(jaxeTextPane);
		
		scrollRes = new JScrollPane (jaxeTextPane);
		scrollRes.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollRes.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		add(scrollRes);
		setSize(800,600);
		setVisible(true);
		
	}
}
