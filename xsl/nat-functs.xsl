<?xml version='1.0' encoding="UTF-8" ?>                                     
<!--                                                                        
 * NAT - An universal Translator                                            
 * Copyright (C) 2005 Bruno Mascret                                         
 * Contact: bmascret@free.fr                                                
 *                                                                          
 * This program is free software; you can redistribute it and/or            
 * modify it under the terms of the GNU General Public License              
 * as published by the Free Software Foundation; either version 2           
 * of the License.                                                          
 *                                                                          
 * This program is distributed in the hope that it will be useful,          
 * but WITHOUT ANY WARRANTY; without even the implied warranty of           
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            
 * GNU General Public License for more details.                             
 *                                                                          
 * You should have received a copy of the GNU General Public License        
 * along with this program; if not, write to the Free Software              
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->
<!DOCTYPE xsl:stylesheet SYSTEM "./mmlents/windob.dtd"
[
   <!ENTITY % table_tbfr PUBLIC "table embosseuse" "./tablesUsed/TbFr2007.ent">
	%table_tbfr;
]>
                                                                               
<xsl:stylesheet version="2.0"                                                     
xmlns:xsl='http://www.w3.org/1999/XSL/Transform'                                  
xmlns:nat='http://natbraille.free.fr/xsl'                                         
xmlns:fn='http://www.w3.org/2005/xpath-functions'                                 
xmlns:xs='http://www.w3.org/2001/XMLSchema'>                                      
<!-- version: 1.0.1 -->                                                           
<!-- <xsl:output method="text" encoding="UTF-8" indent="no"/> -->

<!-- returns the DIRECT text value of a node, not the text value of children -->
<xsl:function name="nat:direct-text" as="xs:string?" xmlns:nat="http://natbraille.free.fr/xsl">
	<xsl:param name="arg" as="element()"/>
	<xsl:variable name="sequence_text" as="xs:string*">
		<xsl:for-each select="$arg/text()">
			<xsl:value-of select="."/>
		</xsl:for-each>
	</xsl:variable>
	<xsl:value-of select="string-join($sequence_text,'')" />
</xsl:function>

<!-- inserts the char $char after each char of $arg if not a '^' or a '$'-->                                      
<xsl:function name="nat:insert-char" as="xs:string" xmlns:nat="http://natbraille.free.fr/xsl" >                   
        <xsl:param name="arg" as="xs:string?"/>                                                                   
        <xsl:param name="char" as="xs:string?"/>
        <xsl:value-of select="string-join(
                for $ch in string-to-codepoints($arg)
                return if (not(codepoints-to-string($ch)='^' or codepoints-to-string($ch)='$'))
                then concat(codepoints-to-string($ch),$char)
                else codepoints-to-string($ch),'')
        "/>
</xsl:function>
<!-- insert the char $char after each chars of $arg except for digits and supresses '$' and '^' -->
<xsl:function name="nat:insert-char-if-not-digit" as="xs:string" xmlns:nat="http://natbraille.free.fr/xsl" >
        <xsl:param name="arg" as="xs:string?"/>
        <xsl:param name="char" as="xs:string?"/>

        <xsl:variable name="tab" select="string-to-codepoints($arg)" as="xs:integer*"/>
        <xsl:variable name="retour" as="xs:string*">
                <xsl:for-each select="$tab">
                        <xsl:variable name="i" select="position()" as="xs:integer"/>
                        <!--<xsl:message select="concat('position:',position(),' actu:',.,' next:',$tab[$i + 1])"/>-->
                        <xsl:value-of select="
                                if (not(string-to-codepoints('^')=. or string-to-codepoints('$')=.))
                                then
                                if (. &lt; string-to-codepoints('9') and . &gt; string-to-codepoints('0')  or
                                        ($tab[$i + 1] &lt; string-to-codepoints('9') and $tab[$i + 1] &gt; string-to-codepoints('0')))
                                then codepoints-to-string(.)
                                else concat(codepoints-to-string(.),$char)
                                else ''
                        "/>
                </xsl:for-each>
        </xsl:variable>
        <!-- retour sous forme de string -->
        <!--<xsl:message select="string-join($retour,'')"/>-->
        <xsl:value-of select="string-join($retour,'')"/>
</xsl:function>
<!-- 
*** Fonction nat:starts-with-any-of ***
* retour: renvoie vrai si $arg commence par une des chaînes de startStrings
* return: true if $arg starts with any of startStrings 
*
* d'après functx:contains-any-of de Priscilla Walmsley-->
<xsl:function name="nat:starts-with-any-of" as="xs:boolean" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:param name="startStrings" as="xs:string*"/> 
 
  <xsl:sequence select=" 
   some $startString in $startStrings
   satisfies starts-with($arg,$startString)
 "/>
   
</xsl:function>

<!-- 
*** Fonction nat:ends-with-any-of ***
* retour: renvoie vrai si $arg se termine par une des chaînes de endStrings
* return: true if $arg ends with any of endStrings 
*
* d'après functx:contains-any-of de Priscilla Walmsley-->
<xsl:function name="nat:ends-with-any-of" as="xs:boolean" 
              xmlns:nat="http://natbraille.free.fr/xsl" >
  <xsl:param name="arg" as="xs:string?"/> 
  <xsl:param name="endStrings" as="xs:string*"/> 
 
  <xsl:sequence select=" 
   some $endString in $endStrings
   satisfies ends-with($arg,$endString)
 "/>
   
</xsl:function>

<!-- 
  Escapes only 2 regex special characters : $ and \
@param   $arg the string to escape 
* d'après functx:contains-any-of de Priscilla Walmsley-->
<xsl:function name="nat:escape-for-regex2" as="xs:string" 
			  xmlns:nat="http://natbraille.free.fr/xsl">
  <xsl:param name="arg" as="xs:string?"/> 
 
  <xsl:sequence select="
  replace($arg,'(\\|\$)','\\$1')  "/>
</xsl:function>

<!--
    transcrit une chaine en braille utf8 vers le braille tbfr2007
    utile pour la lecture à l'écran des fichiers temporaires -->
<xsl:function name="nat:toTbfr" as="xs:string"
			  xmlns:nat="http://natbraille.free.fr/xsl">
  <xsl:param name="arg" as="xs:string?"/>
  
  <xsl:variable name="ptBraille" as="xs:string">
	<xsl:text>&pt;&pt1;&pt12;&pt123;&pt1234;&pt12345;&pt123456;&pt12346;&pt1235;&pt12356;&pt1236;&pt124;&pt1245;&pt12456;&pt1246;&pt125;&pt1256;&pt126;&pt13;&pt134;&pt1345;&pt13456;&pt1346;&pt135;&pt1356;&pt136;&pt14;&pt145;&pt1456;&pt146;&pt15;&pt156;&pt16;&pt2;&pt23;&pt234;&pt2345;&pt23456;&pt2346;&pt235;&pt2356;&pt236;&pt24;&pt245;&pt2456;&pt246;&pt25;&pt256;&pt26;&pt3;&pt34;&pt345;&pt3456;&pt346;&pt35;&pt356;&pt36;&pt4;&pt45;&pt456;&pt46;&pt5;&pt56;&pt6;</xsl:text>
  </xsl:variable>
  <xsl:variable name="ptEmbos" as="xs:string">
	<xsl:text>&pte;&pte1;&pte12;&pte123;&pte1234;&pte12345;&pte123456;&pte12346;&pte1235;&pte12356;&pte1236;&pte124;&pte1245;&pte12456;&pte1246;&pte125;&pte1256;&pte126;&pte13;&pte134;&pte1345;&pte13456;&pte1346;&pte135;&pte1356;&pte136;&pte14;&pte145;&pte1456;&pte146;&pte15;&pte156;&pte16;&pte2;&pte23;&pte234;&pte2345;&pte23456;&pte2346;&pte235;&pte2356;&pte236;&pte24;&pte245;&pte2456;&pte246;&pte25;&pte256;&pte26;&pte3;&pte34;&pte345;&pte3456;&pte346;&pte35;&pte356;&pte36;&pte4;&pte45;&pte456;&pte46;&pte5;&pte56;&pte6;</xsl:text>
  </xsl:variable>
  <!--<xsl:value-of select="translate($arg,concat(string($ptBraille),$carcoupure,$espace),concat(string($ptEmbos),'ECMNTU '))"/>-->
	<xsl:value-of select="translate($arg,$ptBraille,$ptEmbos)"/>
</xsl:function>

<!-- transcription g0 vers BrailleUTF8 -->
<xsl:function name="nat:toBrUTF8" as="xs:string"
			  xmlns:nat="http://natbraille.free.fr/xsl">
  <xsl:param name="arg" as="xs:string?"/>
  
  <xsl:variable name="in" as="xs:string" select="'áíóúñìòäöabcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=.,&nbsp;/&#8209;’‘:'''"/>
  <xsl:variable name="out" as="xs:string">
	<xsl:text>&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt256;&pt2;&pt;&pt34;&pt36;&pt3;&pt3;&pt25;&pt3;</xsl:text>
  </xsl:variable>
  <!--<xsl:value-of select="translate($arg,concat(string($ptBraille),$carcoupure,$espace),concat(string($ptEmbos),'ECMNTU '))"/>-->
	<xsl:value-of select="translate($arg,$in,$out)"/>
</xsl:function>

<!-- detranscription g0 de BrailleUTF8 vers noir-->
<xsl:function name="nat:toBlack" as="xs:string"
			  xmlns:nat="http://natbraille.free.fr/xsl">
  <xsl:param name="arg" as="xs:string?"/>
  
  <xsl:variable name="out" as="xs:string" select="'áíóúñìòäöabcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=.,&nbsp;/&#8209;’‘:'''"/>
  <xsl:variable name="in" as="xs:string">
	<xsl:text>&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt256;&pt2;&pt;&pt34;&pt36;&pt3;&pt3;&pt25;&pt3;</xsl:text>
  </xsl:variable>
  <!--<xsl:value-of select="translate($arg,concat(string($ptBraille),$carcoupure,$espace),concat(string($ptEmbos),'ECMNTU '))"/>-->
	<xsl:value-of select="translate($arg,$in,$out)"/>
</xsl:function>

<!--
    renvoie la taille des colonnes du tableau tab composé de nCol passé en paramètre -->
<xsl:function name="nat:getColSize" as="xs:integer*"
			  xmlns:nat="http://natbraille.free.fr/xsl">
  <xsl:param name="tab" as="xs:string*"/>
	<xsl:param name="nCol" as="xs:integer"/>
	<xsl:sequence select="for $i in 1 to $nCol
  return max(for $c in 1 to count($tab) return 
		if($c mod($nCol) = $i mod($nCol)) then string-length($tab[$c]) else() )"/>
</xsl:function>

 
</xsl:stylesheet>
