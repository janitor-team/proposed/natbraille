/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat;

import java.util.Arrays;
import java.util.List;
import java.io.IOException;
import joptsimple.OptionParser;

import nat.OptNames;
import java.lang.Boolean;
import java.lang.Integer;

/**
 * Classe dérivée de OptionParser
 * <p>Gère le parsage des options de la ligne de commande</p>
 * @author bruno
 *
 */
public class OptionParserNat extends OptionParser
{
	/** liste des chaines possible pour l'option interface graphique*/
    private final List<String> GUI_OPTIONS = Arrays.asList(new String[] { "g", "gui" } );
    /** liste des chaines possible pour l'option aide*/
    private final List<String> HELP_OPTIONS = Arrays.asList(new String[] { "h", "?","help" } );

    /** liste des chaines possible pour l'option fichier source*/
    private final List<String> SOURCE_FILE_OPTIONS = Arrays.asList(new String[] { "f", "from"} );
    /** liste des chaines possible pour l'option fichier cible*/
    private final List<String> CIBLE_FILE_OPTIONS = Arrays.asList(new String[] { "t", "to" } );
    /** liste des chaines possible pour l'option fichier de configuration*/
    private final List<String> CONFIG_FILE_OPTIONS = Arrays.asList(new String[] { "c", "config"} );
    
    /** liste des chaines possible pour l'option silence*/
    private final List<String> QUIET_OPTIONS = Arrays.asList(new String[] { "q", "quiet"} );
    
    /** Constructeur */
    public OptionParserNat()
    {
		acceptsAll(GUI_OPTIONS, "loads graphical interface" );
		acceptsAll(HELP_OPTIONS, "displays help message" );
		acceptsAll(QUIET_OPTIONS, "no console output" );
		/*
		  acceptsAll(VERBOSE_OPTIONS, "mode bavard" ).withRequiredArg().describedAs("count" ).ofType(Integer.class );
		*/
		acceptsAll(CONFIG_FILE_OPTIONS, "sets configuration file").withRequiredArg().describedAs("sourcefile").ofType(String.class);
		acceptsAll(SOURCE_FILE_OPTIONS, "sets source file").withRequiredArg().describedAs("sourcefile").ofType(String.class);
		acceptsAll(CIBLE_FILE_OPTIONS, "sets destination file").withRequiredArg().describedAs("destfile").ofType(String.class);
	
		// debug
		accepts(OptNames.ge_log_verbosity,"debug verbosity").withRequiredArg().describedAs("verbosity level 1|2|3").ofType(Integer.class);
		    
		// ui - editor
		/*
		  accepts(OptNames.ui_editor_font_size,"font size for braille editor").withRequiredArg().describedAs("size").ofType(Integer.class);
		  accepts(OptNames.ui_editor_zone2_display,"").withRequiredArg().describedAs("size").ofType(String.class);
		  accepts(OptNames.ui_editor_font2,"").withRequiredArg().describedAs("").ofType(String.class) ;
		  accepts(OptNames.ui_editor_font2_size,"").withRequiredArg().describedAs("").ofType(Integer.class) ;
		  accepts(OptNames.ui_editor_auto_open,"").withRequiredArg().describedAs("").ofType(Boolean.class) ;
		  accepts(OptNames.ui_editor_external,"").withRequiredArg().describedAs("").ofType(String.class) ;
		*/		
	
		// todo : get et set dans confignat et relier à l'utilisation (pour l'instant : constant"
		//	accepts(OptNames.ui_filter_conf_dir,"").withRequiredArg().describedAs("").ofType(String.class) ;
		
		// last in and out document file
		/*
		  accepts(OptNames.last_source_filename ,"").withRequiredArg().describedAs("").ofType(String.class);
		  accepts(OptNames.last_source_encoding,"").withRequiredArg().describedAs("").ofType(String.class) ;
		  accepts(OptNames.last_dest_filename,"").withRequiredArg().describedAs("").ofType(String.class) ;
		  accepts(OptNames.last_dest_encoding,"").withRequiredArg().describedAs("").ofType(String.class) ;
		  accepts(OptNames.last_filter_configuration_filename,"").withRequiredArg().describedAs("").ofType(String.class) ;
		*/
	
		// filters
		//	accepts(OptNames.fi_dtd_filename,"sets NAT dtd").withRequiredArg().describedAs("filename").ofType(String.class);
		accepts(OptNames.fi_braille_table ,"sets braille table").withRequiredArg().describedAs("").ofType(String.class);
		//	accepts(OptNames.fi_filter_filename,"sets filter filename").withRequiredArg().describedAs("").ofType(String.class) ; // xsl
		/*
		accepts(OptNames.fi_tag_doc_begin,"tag to be added before document").withRequiredArg().describedAs("tag").ofType(String.class) ;
		accepts(OptNames.fi_tag_doc_end,"tag to be added after document").withRequiredArg().describedAs("tag").ofType(String.class) ;
		accepts(OptNames.fi_tag_par_begin,"tag to be added before each paragraph").withRequiredArg().describedAs("tag").ofType(String.class) ;
		accepts(OptNames.fi_tag_par_end,"tag to be added after each paragraph").withRequiredArg().describedAs("tag").ofType(String.class) ;
		*/
		// maths filter
		accepts(OptNames.fi_math_transcribe,"transcribe math contents").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
		//	accepts(OptNames.fi_math_filter_filename,"sets math filter").withRequiredArg().describedAs("filename").ofType(String.class) ;
		accepts(OptNames.fi_math_use_trigo_spec,"use special trigonmetric notation").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
	
		/*
		accepts(OptNames.fi_math_tag_deb,"tag to be added before math contents").withRequiredArg().describedAs("tag").ofType(String.class) ;
		accepts(OptNames.fi_math_tag_end,"tag to be added after math contents").withRequiredArg().describedAs("tag").ofType(String.class) ;
		*/
		// litt filter
	
		accepts(OptNames.fi_litt_transcribe,"transcribe litterary contents").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
		//	accepts(OptNames.fi_litt_fr_int_filter_filename,"sets integral litteray filter").withRequiredArg().describedAs("filename").ofType(String.class) ;
		//	accepts(OptNames.fi_litt_fr_abbreg_filter_filename,"sets abbreged filter").withRequiredArg().describedAs("filename").ofType(String.class) ;
		accepts(OptNames.fi_litt_abbreg,"transcribe as abbreged").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
		/*
		accepts(OptNames.fi_litt_tag_deb,"tag to be added before litterary contents").withRequiredArg().describedAs("tag").ofType(String.class) ;
		accepts(OptNames.fi_litt_tag_end,"tag to be added after litterary contents").withRequiredArg().describedAs("tag").ofType(String.class) ;
		*/
		// music filter
		accepts(OptNames.fi_music_transcribe,"transcribe musical contents").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
		//	accepts(OptNames.fi_music_filter_filename,"music filter").withRequiredArg().describedAs("filename").ofType(String.class) ;
		/*
		accepts(OptNames.fi_music_tag_deb,"tag to be added before musical contents").withRequiredArg().describedAs("tag").ofType(String.class) ;
		accepts(OptNames.fi_music_tag_end,"tag to be added after musical contents").withRequiredArg().describedAs("tag").ofType(String.class) ;
		*/	
		// hyphenation
		accepts(OptNames.fi_hyphenation,"use hyphenation").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
		accepts(OptNames.fi_line_lenght,"sets line length").withRequiredArg().describedAs("char number").ofType(Integer.class) ;
		accepts(OptNames.fi_hyphenation_dirty,"use dirty hyphenation").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
		
		// emboss & print
		//	accepts(OptNames.pr_emboss_table,"sets embossing table").withRequiredArg().describedAs("table name").ofType(String.class) ;
		//	accepts(OptNames.pr_use_emboss_command,"emboss with an external command").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
		//	accepts(OptNames.pr_emboss_command,"sets embossing command").withRequiredArg().describedAs("command").ofType(String.class) ;
		//	accepts(OptNames.pr_emboss_auto,"sets auto emboss mode after the document has been transcribed").withRequiredArg().describedAs("true/false").ofType(Boolean.class) ;
		//	accepts(OptNames.pr_emboss_print_service,"sets embossing printing service").withRequiredArg().describedAs("service").ofType(String.class) ;
		//	accepts(OptNames.pr_os,"sets os name").withRequiredArg().describedAs("name").ofType(String.class) ;
	
		// encoding
		accepts(OptNames.en_in,"input file encoding").withRequiredArg().describedAs("encoding name").ofType(String.class);
		accepts(OptNames.en_out,"output file encoding").withRequiredArg().describedAs("encoding name").ofType(String.class);
    }
    /**
     * Affiche l'aide sur les options en lignes de commande
     * @throws IOException erreur d'entrée sortie lors de l'affichage de l'aide
     */
    public void printCliUsage() throws IOException{	printHelpOn(System.out );}
}
