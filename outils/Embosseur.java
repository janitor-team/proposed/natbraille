/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package outils;

import gestionnaires.GestionnaireErreur;
/**
 * Classe abstraite permettant la gestion de l'embossage
 * @author bruno
 *
 */
public abstract class Embosseur 
{
	/** L'adresse du fichier à embosser */
	protected String fichier;
	/** une instance de {@link GestionnaireErreur}*/
	protected GestionnaireErreur gest;
	/** Feuille xsl permettant la conversion vers la table braille d'embossage */
	protected String filtre = "xsl/impression.xsl";
	//private String fichImp = "tmpImp.txt";
	//private String fichImpSrc = "tmpImpSrc.txt"; 
	
	//protected ConfigNat confNat;
	/**
	 * Constructeur
	 * @param f L'adresse du fichier à embosser
	 * @param g une instance de {@link GestionnaireErreur}
	 */
	public Embosseur(String f, GestionnaireErreur g)
	{
		fichier = f;
		gest= g;
	}
	
	/** 
	 * Méthode d'accès en lecture à {@link #gest} 
	 * @return le gestionnaire d'erreur utilisé
	 */
	public GestionnaireErreur getGest() {return gest ;}
	
	/*public boolean convertiTable()
	{
		boolean retour = false;
		if(ajouteEntete())
		{
			gest.AfficheMessage("ok\n*** Création de la fabrique (DocumentBuilderFactory) ...",Nat.LOG_VERBEUX);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			//configuration de la fabrique
			factory.setNamespaceAware(false);
			factory.setValidating(false);//je mets a false: pas besoin de réseau: non, voir xhtmlDocument
			//factory.setIgnoringElementContentWhitespace(true);
			factory.setIgnoringComments(true);
			factory.setIgnoringElementContentWhitespace(false);
			
			try 
			{
				DocumentBuilder builder = factory.newDocumentBuilder();
				gest.AfficheMessage("ok\n*** Parsage du document teste d'entrée avec SAX ...",Nat.LOG_VERBEUX);
				builder.setErrorHandler(gest);
				Document doc = builder.parse(new File(fichImpSrc));
				doc.setStrictErrorChecking(true);
				gest.AfficheMessage("ok\n*** Initialisation et lecture de la feuille de style de conversion...",Nat.LOG_VERBEUX);
				TransformerFactory transformFactory = TransformerFactory.newInstance();
				StreamSource styleSource = new StreamSource(new File(filtre));
				// lire le style
				
				Transformer transform = transformFactory.newTransformer(styleSource);
				// conformer le transformeur au style
				DOMSource in = new DOMSource(doc);
				gest.AfficheMessage("ok\n*** Création du fichier d'impression ...",Nat.LOG_VERBEUX);
				// Création du fichier de sortie
				File file = new File(fichImp);
				StreamResult out = new StreamResult(file);
				gest.AfficheMessage("ok\n*** Transformation du fichier d'entrée pour l'impression...",Nat.LOG_VERBEUX);
				transform.transform(in, out);
				retour = true;
			}
			catch (Exception e)  
			{
				gest.setException(e);
				gest.GestionErreur();
			}
		}
		return retour;
	}
	
	private boolean ajouteEntete()
	{
		boolean retour = false;
		try
		{
			gest.AfficheMessage("\n** Ajout des entêtes et création fichier temp impression", Nat.LOG_VERBEUX);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichImpSrc),"UTF-8"));
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fichier),ConfigNat.getCurrentConfig().getSortieEncoding()));
			//ajout entete
			bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?><textimp>");
			String line = br.readLine();
			while(line != null)
			{
				bw.write(line+"\n");
				line = br.readLine();
			}
			bw.write("</textimp>");
			br.close();
			bw.close();
			retour = true;
		}
		catch (IOException e)
		{
			System.err.println("erreur dans: " + e);
			gest.AfficheMessage("Erreur entrée/sortie ajout entête impression", Nat.LOG_SILENCIEUX);
		}
		return retour;
	}*/
	/** Méthode à redéfinir; embosse le fichier {@link #fichier}*/
	public abstract void Embosser();
	/**
	 * Méthode à redéfinir; embosse le fichier dont l'adresse est donnée en paramètre
	 * @param f l'adresse du fichier à embosser
	 */
	public abstract void Embosser(String f);
}
