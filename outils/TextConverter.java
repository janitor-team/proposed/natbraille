/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/**
* A class for conversion of text file from
* UNIX to Windows.
*/

package outils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Classe permettant de convertir un fichier texte unix au format windows.
 * @author bruno
 *
 */
public class TextConverter
{
	/** fichier à convertir */
	private File file;
	/** 
	 * Constructeur
	 * @param s l'adresse du fichier à convertir
	 */
	public TextConverter (String s) 
	{
		file= new File (s);
	}
	/**
	 * Convertit le fichier unix {@link #file} au format windows
	 * <p>Ajoute un byte 13 devant chaque byte 10</p>
	 * @throws FileNotFoundException <p>si {@link #file} n'existe pas</p>
	 * @throws IOException <p>si problème d'écriture ou de lecture du fichier</p> 
	 */
	public void convert() throws FileNotFoundException, IOException
	{
		if (file.exists())
		{
			FileInputStream reader= new FileInputStream (file);
			File result= new File(file.getPath()+".txt");
			if(result.exists()){result.delete();}
			result.createNewFile();
			FileOutputStream writer= new FileOutputStream (result);
			int red= reader.read();
			while (red != -1)
			{
				// inserts a byte 13 before 10
				if (red==10) {writer.write(13);}
				// writes the current byte
				writer.write(red);
				// next byte is red.
				red= reader.read();
			}
			
			reader.close();
			writer.close();
			
			// deletes the old file
			file.delete();
			// renames the new one
			result.renameTo(file);
		}
	}
}

