/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.presentateur;

/*import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;*/

import java.io.File;
import java.util.Arrays;
import outils.FileToolKit;

import nat.ConfigNat;
import nat.Nat;
import gestionnaires.GestionnaireErreur;
/**
 * Effectue un rendu basique du fichier transcrit
 * <p>Se contente d'encoder correctement le fichier de sortie</p>
 * @author bruno
 *
 */
public class PresentateurSans extends Presentateur
{
	/** encodage du fichier de sortie */
	protected String charset;
	/**
	 * Constructeur
	 * @param g une instance de {@link GestionnaireErreur}
	 * @param src l'adresse du fichier transcrit à présenter
	 * @param tgt l'adresse de la sortie 
	 * @param chSet encodage du fichier de sortie
	 * @param tab la table braille de sortie
	 */
	public PresentateurSans(GestionnaireErreur g, String chSet, String src,String tgt, String tab)
	{
		super(g,src,tgt, tab);
		//this.gest = gest;
		charset = chSet;
	}
	/**
	 * Encode le fichier d'entrée avec la table braille et l'encodage de sortie
	 * @return true si encodage réussi
	 */
	protected boolean encode()
	{
		//encodage dans le charset et la table braille désirés
		return FileToolKit.convertBrailleFile(source, sortie,ConfigNat.getInstallFolder()+"xsl/tablesUsed/brailleUTF8.ent",tableBraille,"UTF-8", charset, gest);
	}
	/**
	 * Rédéfinition de {@link Presentateur#presenter()}
	 * <p>Se contente pour produire le rendu d'encoder le fichier source avec l'encodage du fichier de sortie</p> 
	 */
	@Override
	public boolean presenter()
	{
		Boolean ok = true;
		tempsExecution = System.currentTimeMillis();
		gest.afficheMessage("\nDébut de la mise en forme du document ... ok\n",Nat.LOG_SILENCIEUX);
		addImages(source);
		ok = encode();
		if (ok)
			{ gest.afficheMessage("ok\n** mise en forme ...",Nat.LOG_NORMAL);
			tempsExecution = System.currentTimeMillis() - tempsExecution; }
		return ok;
	}
	
	/**
	 * Ajoute les images en annexes si l'option est activée
	 * @param fic l'adresse du fichier recevant les images
	 */
	protected void addImages(String fic)
	{
		if(ConfigNat.getCurrentConfig().getTranscrireImages())
		{
			//ajout des images en annexe
			gest.afficheMessage("\n*** Incorporation des images en annexe...",Nat.LOG_VERBEUX);
			File repertoire =new File(ConfigNat.getUserTempFolder()+"tmp.xhtml-img");
			if(repertoire.isDirectory())
			{
				gest.afficheMessage("\n*** Il y a des images...",Nat.LOG_VERBEUX);
				File[] listImages = repertoire.listFiles();
				
				String images=FileToolKit.loadFileToStr(fic);
				images = images.substring(0, images.length()-1);//je vire le dernier saut de ligne
				int j=0;
				Arrays.sort(listImages);
				for(File f:listImages)
				{
					if(f.getName().endsWith(".brf"))
					{
						//avec suppression des 3 premières lignes
						String newImage =  FileToolKit.loadFileToStr(f.getAbsolutePath()).split("\n", 4)[3];
						//faut-il ajouter des lignes? 
						for(int i=newImage.split("\n").length;i<ConfigNat.getCurrentConfig().getNbLigne()-1;i++)
						{
							newImage += "\n";
						}
						
						images += newImage;
						//numérotation de l'annexe (en bas à gauche) et saut de page
						j++;
						images += getNumBraille(j)+"\n"+(char)12;
					}
				}
				FileToolKit.saveStrToFile(images, fic, "UTF-8");
			}
		}
		
	}
	
	/**
	 * Renvoit la valeur de l'entier i en Braille avec le préfixe pt6 dans la table Braille UTF8
	 * @param i l'entier à convertir en Braille
	 * @return chaine Braille correspondant à l'entier <code>i</code>
	 */
	protected String getNumBraille(int i)
	{
		return "\u2820"+(""+i).replaceAll("1", "\u2821").replaceAll("2", "\u2823").replaceAll("3", "\u2829").replaceAll("4", "\u2839")
			.replaceAll("5", "\u2831").replaceAll("6", "\u282B").replaceAll("7", "\u283B").replaceAll("8", "\u2833")
			.replaceAll("9", "\u282A").replaceAll("0", "\u283C");
	}
}