@echo off
echo *** Compilation complete de NAT ***
echo --- Package Writer2latex ---
javac -source 5 -target 5 -encoding utf8 writer2latex/xhtml/*.java
echo.
echo --- Packages de Nat ---
javac -classpath .;lib/jeuclid-core-3.1.9.jar;lib/jeuclid-fop-3.1.9.jar;lib/saxon9he.jar;lib/saxon9-dom.jar;lib/xercesImpl-2.9.1.jar;lib/log4j-1.2.14.jar;lib/jing.jar;lib/jazzy.jar;lib/fop_and_libs.jar;lib/commons-io-1.4.jar;lib/jodconverter-2.2.2.jar;lib/jurt-3.0.1.jar;lib/slf4j-api-1.5.6.jar;lib/unoil-3.0.1.jar;lib/juh-3.0.1.jar;lib/ridl-3.0.1.jar;lib/slf4j-jdk14-1.5.6.jar;lib/xstream-1.3.1.jar;lib/jhall.jar;lib/saxon9.jar -source 6 -target 6 -encoding utf8 gestionnaires/*.java ui/*.java nat/transcodeur/*.java nat/saxFuncts/*.java nat/presentateur/*.java nat/convertisseur/*.java nat/*.java outils/*.java joptsimple/*.java joptsimple/internal/*.java joptsimple/util/*.java org/im4java/core/*.java org/im4java/core/*.java org/im4java/process/*.java org/im4java/utils/*.java"
echo.
pause
