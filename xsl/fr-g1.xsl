<?xml version='1.0' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->

<!DOCTYPE xsl:stylesheet SYSTEM "mmlents/windob.dtd">


<xsl:stylesheet version="2.0"
xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:include href="fr-commun.xsl"/>

<!-- traitement particulier sur le littéraire; en intégral: rien -->
<xsl:template match="lit">
	<!--<xsl:text>debLit</xsl:text>-->
	<xsl:apply-templates select="@*|*|text()|processing-instruction()" />
	<!--<xsl:text>FinLit</xsl:text>-->
</xsl:template>
<!-- ***************************************************
		SYMBOLES COMPOSES
	*************************************************
	Les entités de coupures ont été placées un peu au pif, en fonction de ce que je pensais probable
	Règles utilisées:
	1) il faut empêcher une coupure de symbole composé => utiliser au minimu un symbole de coupure
	dans l'idéal, encadrer le symbole par deux entités de coupure.
	2) certains symboles ne doivent pas être séparés de ce qui les précède (€, °...) ou de ce qui les suit
	(pas trouvé d'exemple; le $, si on respecte la notation americaine: $3 et non pas 3$).
	Dans ce cas, une seule entité de coupure est ajoutée après le symbole.
-->

<xsl:template name="symbolesComposes">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer"/>
	<xsl:param name="tiret" select='0' as="xs:integer"/><!-- = 1 si il faut tester le tiret simple; 2 si on est en fin de mot-->
	<xsl:choose>
		<xsl:when test="contains(translate($mot,'-&ndash;&mdash;©°§®&trade;&amp;~*#%‰_\¢€£$¥&lt;&gt;≤≥²³&sup2;&sup3;&alpha;&beta;&gamma;&delta;&epsi;&epsiv;&zeta;&eta;&theta;&thetav;&iota;&kappa;&lambda;&mu;&nu;&xi;&omicron;&pi;&piv;&rho;&rhov;&sigma;&sigmav;&tau;&upsilon;&phi;&phiv;&chi;&psi;&omega;&Agr;&Bgr;&Gamma;&Delta;&Egr;&Zgr;&EEgr;&Theta;&Igr;&Kgr;&Lambda;&Mgr;&Ngr;&Xi;&Ogr;&Pi;&Rgr;&Sigma;&Tgr;&Upsilon;&Phi;&KHgr;&Chi;&Psi;&Omega;','aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'),'a')">
			<xsl:choose>
				<xsl:when test="not($tiret=2) and substring($mot,1,1)='-' and (string-length(translate($mot,$l_num,''))=0 or $tiret=1)">
					<xsl:text>&pt36;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='&mdash;' or substring($mot,1,1)='&ndash;' or substring($mot,1,1)='-'"> <!-- tirets long et moyen cadratin -->
					<xsl:text>&pt36;&pt36;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='©'">
					<xsl:text>&pt5;&pt14;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='°'">
					<xsl:text>&pt5;&pt135;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='§'">
					<xsl:text>&pt5;&pt1234;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='®'">
					<xsl:text>&pt5;&pt1235;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='&trade;'">
					<xsl:text>&pt5;&pt2345;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='&amp;'">
					<xsl:text>&pt5;&pt123456;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='~'">
					<xsl:text>&pt5;&pt26;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='*'">
					<xsl:text>&pt5;&pt35;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='#'">
					<xsl:text>&pt5;&pt3456;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='%'">
					<xsl:text>&pt5;&pt346;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='‰'">
					<xsl:text>&pt5;&pt346;&pt346;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='_'">
					<xsl:text>&pt5;&pt36;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='\'">
					<xsl:text>&pt5;&pt34;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='¢'">
					<xsl:text>&pt45;&pt14;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='€'">
					<xsl:text>&pt45;&pt15;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='£'">
					<xsl:text>&pt45;&pt123;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='$'">
					<xsl:text>&pt45;&pt234;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='¥'">
					<xsl:text>&pt45;&pt13456;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='&lt;'">
					<xsl:text>&pt5;&pt126;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='&gt;'">
					<xsl:text>&pt5;&pt345;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='≤'">
					<xsl:text>&pt45;&pt126;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='≥'">
					<xsl:text>&pt45;&pt345;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='²' or substring($mot,1,1)='&sup2;'">
					<xsl:text>&pt4;&pt126;</xsl:text>
				</xsl:when>
				<xsl:when test="substring($mot,1,1)='³' or substring($mot,1,1)='&sup3;'">
					<xsl:text>&pt4;&pt146;</xsl:text>
				</xsl:when>
				<xsl:when test="contains($l_grec_min,substring($mot,1,1))">
					<xsl:text>&pt45;</xsl:text>
					<xsl:value-of select="translate(substring($mot,1,1),$l_grec_min,'&pt1;&pt12;&pt1245;&pt145;&pt15;&pt15;&pt1356;&pt125;&pt245;&pt245;&pt24;&pt13;&pt123;&pt134;&pt1345;&pt1346;&pt135;&pt1234;&pt12456;&pt1235;&pt1235;&pt234;&pt234;&pt2345;&pt136;&pt124;&pt124;&pt12345;&pt13456;&pt2456;')"/>
				</xsl:when>
				<!-- clef &pt46;&pt45; : lettres grecques majuscules-->
				<xsl:when test="contains($l_grec_maj,substring($mot,1,1))">
					<xsl:text>&pt46;&pt45;</xsl:text>
					<xsl:value-of select="translate(substring($mot,1,1), $l_grec_maj,'&pt1;&pt12;&pt1245;&pt145;&pt15;&pt1356;&pt125;&pt245;&pt24;&pt13;&pt123;&pt134;&pt1345;&pt2456;&pt135;&pt1234;&pt1235;&pt234;&pt2345;&pt136;&pt124;&pt12345;&pt12345;&pt13456;&pt2456;')"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="bijection">
						<xsl:with-param name="mot" select="substring($mot,1,1)"/>
						<xsl:with-param name="PN" select="$PN"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="string-length($mot) &gt; 1">
				<xsl:choose>
					<xsl:when test="string-length($mot) = 2">
						<xsl:call-template name="symbolesComposes">
							<xsl:with-param name="mot" select="substring($mot,2,string-length($mot))"/>
							<xsl:with-param name="PN" select="$PN"/>
							<xsl:with-param name="tiret" select='2'/>
						</xsl:call-template>
					</xsl:when>
					<xsl:otherwise>
						<xsl:call-template name="symbolesComposes">
							<xsl:with-param name="mot" select="substring($mot,2,string-length($mot))"/>
							<xsl:with-param name="PN" select="$PN"/>
							<xsl:with-param name="tiret" select='1'/>
						</xsl:call-template>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="$mot"/>
				<xsl:with-param name="PN" select="$PN"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="bijection">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:call-template name="finBijection">
		<xsl:with-param name="mot" select="$mot"/>
		<xsl:with-param name="PN" select="$PN"/>
	</xsl:call-template>
</xsl:template>

</xsl:stylesheet>
