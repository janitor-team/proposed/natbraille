<?xml version='1.0' encoding="UTF-8" ?>
<!--
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
-->

<!-- version: 1.3-b3 -->

<!DOCTYPE xsl:stylesheet SYSTEM "mmlents/windob.dtd">


<xsl:stylesheet version="2.0" xmlns:xsl='http://www.w3.org/1999/XSL/Transform' 
xmlns:saxon='http://icl.com/saxon'
xmlns:xs='http://www.w3.org/2001/XMLSchema'
xmlns:fn='http://www.w3.org/2005/xpath-functions'
xmlns:fo='http://www.w3.org/1999/XSL/Format'
xmlns:m='http://www.w3.org/1998/Math/MathML'
xmlns:functx='http://www.functx.com'
xmlns:lit='espacelit'
xmlns:doc='espaceDoc'>

<xsl:output method="xml" encoding="UTF-8" indent="yes"/>

<xsl:template match="ponctuation[@type='puce']">
	<xsl:text>&pt36;&pt36;</xsl:text>	
</xsl:template>

<xsl:template match="ponctuation[@type='ordre']">
	<xsl:text>&pt6;</xsl:text>
	<xsl:call-template name="bijection">
		<xsl:with-param name="mot" select="@num" />
	</xsl:call-template>
	<xsl:text>&pt36;&pt36;</xsl:text>	
</xsl:template>

<xsl:template match="ponctuation">
	<xsl:choose>
		<!-- chat braille -->
		<xsl:when test=".=')' and (string(preceding::*[1])='B-')" />
		<!-- pts de suspension; -->
		<xsl:when test=".='...' or .='…' or .='&mldr;' or .='&vellip;' or .='&hellip;' or .='&dtdot;' or .='&ldots;' or .='&ctdot;' or .='&utdot;'">
			<xsl:text>&pt256;&pt256;&pt256;</xsl:text>
		</xsl:when>
		<xsl:when test='.="&apos;"'>
			<xsl:text>&pt3;</xsl:text>
		</xsl:when>
		<xsl:when test=".='['">
			<xsl:text>&pt45;&pt236;</xsl:text>
		</xsl:when>
		<xsl:when test=".=']'">
			<xsl:text>&pt356;&pt12;</xsl:text>
		</xsl:when>
		<xsl:when test=".='{'">
			<xsl:text>&pt6;&pt6;&pt236;</xsl:text>
		</xsl:when>
		<xsl:when test=".='}'">
			<xsl:text>&pt356;&pt3;&pt3;</xsl:text>
		</xsl:when>
		<xsl:when test=".='*'">
			 <xsl:text>&pt5;&pt35;</xsl:text>
		</xsl:when>
		<xsl:when test=".='-'">
			<!-- est-ce le signe mathématique - ou un tiret? -->
			<xsl:choose>
				<!-- il y a des numériques avant et/ou après, ou il y a d'autres opérateurs mathématiques (+ ou = ) dans la phrase-->
				<xsl:when test="(number(translate(string(preceding-sibling::*[1]),',0','.1')) or number(translate(string(following-sibling::*[1]),',0','.1'))) or contains(translate(string(..),'+','='),'=')"><!-- conversion des , en . et des 0 en 1 car number(0) et number(NNN,NNN) renvoie faux -->
					<xsl:text>&pt6;</xsl:text>
				</xsl:when>
				<xsl:otherwise><!-- test=".=../child::*[1]">-->
					<xsl:text>&pt36;</xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:text>&pt36;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<!-- utilisation d'une variable pour éviter les pb du translate et des '" -->
			<xsl:variable name="v" as="xs:string">
				<xsl:text>&pt2;&pt23;&pt25;&pt256;&pt26;&pt235;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt2356;&pt3;&pt3;&pt3;&pt3;&pt236;&pt356;&pt235;&pt26;</xsl:text>
			</xsl:variable>
			<xsl:value-of select="translate(.,',;:.?!&quot;«»“”‘’&lsquo;&rsquo;&acute;&prime;()¡¿
',$v)"/>
		</xsl:otherwise>
	</xsl:choose><!--
	<xsl:if test="not (local-name(following::*[1])='phrase') and not(contains('([{«“‘&lsquo;',.)) and not(local-name(following::*[1])='ponctuation')"> RAF:à améliorer pour les guillemets et apos ouvrantssimples 
		<xsl:text> </xsl:text>
	</xsl:if>-->
	<xsl:if test="not (local-name(following::*[1])='phrase') and not(contains('¡¿
([{«“‘&lsquo;',.))"><!-- RAF:à améliorer pour apos ouvrantssimples -->
		<xsl:call-template name="espace"/>
	</xsl:if>
</xsl:template>

<xsl:template match="mot">
	<!-- ****************** Exposant? *************************-->
	<xsl:if test="@attr = 'exp'">
		<xsl:text>&pt4;</xsl:text>
	</xsl:if>
	<xsl:if test="@attr = 'ind'">
		<xsl:text>&pt26;</xsl:text>
	</xsl:if>
	<!-- **************Mises en évidence ***********************-->
	<xsl:if test="(@attr = 'emph' or @attr = 'emph2' or @attr = 'gras' or @attr = 'italique') and ($emph_w or $emph_part)">
		<!-- on vérifie si on est dans un passage; c'ets le même genre d'algo que pour les majuscules -->
		<xsl:choose>
			<!-- applique-t-on la règle du passage ? -->
			<xsl:when test="not($emph_part)">
				<!-- non -->
				<xsl:text>&pt456;</xsl:text>
			</xsl:when>
			<xsl:otherwise><!-- on applique la règle du passage -->
				<xsl:variable name="position1MotME">
					<xsl:call-template name="donnePosition1MotME">
						<xsl:with-param name="position" select="1" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="positionDMotME">
					<xsl:call-template name="donnePositionDMotME">
						<xsl:with-param name="position" select="1" />
					</xsl:call-template>
				</xsl:variable>
				<xsl:choose>
					<!-- le mot est-il dans un passage en évidence? -->
					<!-- c'est le 1er et le dernier mot d'un passage en évidence -->
					<xsl:when test="$position1MotME ='1' and $positionDMotME = '1'">
					<!-- on n'est pas dans un passage en évidence -->
						<xsl:if test="$emph_w">
							<xsl:text>&pt456;</xsl:text>
						</xsl:if>
					</xsl:when>
					<xsl:when test="$position1MotME = '1'">
						<!-- y a-t-il au moins 3 autres mots suivants en évidence?-->
						<xsl:variable name="premierMotPassageME">
							<xsl:call-template name="estSuiviME">
								<xsl:with-param name="nb" select="3"/>
								<xsl:with-param name="position" select="1"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$premierMotPassageME = '1'">
								<xsl:text>&pt25;&pt456;</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="$emph_w">
									<xsl:text>&pt456;</xsl:text>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:when>
					<xsl:when test="$positionDMotME = '1'">
					<!-- dernier mot -->
						<xsl:text>&pt456;</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<!-- ni le premier ni le dernier, est-ce un passage? -->
						<xsl:variable name="nbSuivant">
							<xsl:call-template name="estSuiviME">
								<xsl:with-param name="nb" select="2"/>
								<xsl:with-param name="position" select="1"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="nbPrec">
							<xsl:call-template name="estPrecedeME">
								<xsl:with-param name="nb" select="2"/>
								<xsl:with-param name="position" select="1"/>
							</xsl:call-template>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="$nbSuivant + $nbPrec = 0 and $emph_w">
							<!-- passage pas en évidence -->
								<xsl:text>&pt456;</xsl:text>
							</xsl:when>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:if>
	
	<xsl:choose>
		<xsl:when test="string-length(normalize-space(.))=0"/>
		<!-- chat braille
		<xsl:when test=".='B-' and following::*[1]=')'">
			<xsl:text>&pt46;&pt13;&pt25;&pt135;</xsl:text>
		</xsl:when>-->
		<!-- numérique (ou trait d'union) -->
		<!-- il y a des numériques avant et/ou après, ou il y a d'autres opérateurs mathématiques (+ ou = ) dans la phrase-->
		<xsl:when test=" substring(.,1,1)='-' and ((number(translate(string(preceding-sibling::*[1]),',0','.1')) or number(translate(string(following-sibling::*[1]),',0','.1'))) or contains(translate(string(..),'+','='),'='))">
			<xsl:call-template name="numerique">
				<xsl:with-param name="mot" select="." />
				<xsl:with-param name="position" select="1" />
				<xsl:with-param name="positionPrec" select="0" />
				<xsl:with-param name="PNS" select="3" />
			</xsl:call-template>
		</xsl:when>
		
		<xsl:when test="contains(translate(.,'0123456789+×÷=&sup2;&sup3;','1111111111111111'),'1')">
			<xsl:call-template name="numerique">
				<xsl:with-param name="mot" select="." />
				<xsl:with-param name="position" select="1" />
				<xsl:with-param name="positionPrec" select="0" />
				<xsl:with-param name="PNS" select="3" />
			</xsl:call-template>
		</xsl:when>
		
		<xsl:otherwise>
			<xsl:call-template name="majuscule">
				<xsl:with-param name="mot" select="." />
				<xsl:with-param name="prefixe" select="0" />
				<xsl:with-param name="position" select="1" />
			</xsl:call-template>
			<xsl:call-template name="espace"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="numerique">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="positionPrec" as="xs:integer" select="-10"/>
	<xsl:param name="position" as="xs:integer" select="-10"/>
	<xsl:param name="PNS" as="xs:integer" select="-10"/>
	<xsl:param name="PNActif" as="xs:integer" select="-10"/>
	<xsl:choose>
		<xsl:when test="fn:matches(.,'^(\+|\*|:|=|×|÷)$') and functx:contains-any-of(..,('=','+','×','÷'))"><!-- conversion des , en . et des 0 en 1 car number(0) et number(NNN,NNN) renvoie faux -->
			<xsl:text>&pt6;</xsl:text>
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="$mot"/>
			</xsl:call-template>
			<xsl:call-template name="espace"/>
		</xsl:when>
		<!-- on n'a pas encore lu tout le mot -->
		<xsl:when test="$position &lt; string-length($mot) + 1">
			<xsl:choose>
				<!-- c'est un contenu numérique -->
				<!-- il n'y a plus que des chiffres -->
				<xsl:when test="string-length(translate($mot,$l_num,''))=0">
					<xsl:call-template name="numerique">
						<xsl:with-param name="mot" select="$mot" />
						<xsl:with-param name="position" select="string-length($mot) + 1" />
						<xsl:with-param name="positionPrec" select="1" />
						<xsl:with-param name="PNS" select="1"/>
						<xsl:with-param name="PNActif" select="$PNActif"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="contains($l_num,substring($mot,$position,1))">
					<xsl:choose>
						<!-- il y a autre chose que des chiffres -->
						<!--
						<xsl:when test="(contains('+-×÷=',substring($mot,$position,1)) or $PNS=0) and not(number(translate(substring-before($mot,$position),',0&sup2;&sup3;','.111')))">
						-->
						<xsl:when test="$PNS=0">
							<xsl:if test="not($position = 1)">
								<!-- on sépare la chaine en deux avant le signe num -->
								<xsl:call-template name="majuscule">
									<xsl:with-param name="mot" select="substring($mot,1,$position -1)" />
									<xsl:with-param name="prefixe" select="0" />
									<xsl:with-param name="position" select="1" />
									<xsl:with-param name="PN" select="1"/>
								</xsl:call-template>
								<xsl:if test="not(contains('&sup2;&sup3;',substring($mot,$position,1)))">
									<xsl:text>&pt6;</xsl:text>
								</xsl:if>
							</xsl:if>
							<xsl:choose>
								<!-- y a-t-il ambiguïté possible après? --><!-- ajouter test majuscule-->
								<xsl:when test="contains(substring(translate($mot,$l_amb,$l_amb_ahat),$position + 1,string-length($mot)-$position +1),'â')">
									<!-- on transcrit le numérique -->
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="substring($mot,$position,1)" />
										<xsl:with-param name="prefixe" select="0" />
										<xsl:with-param name="position" select="1" />
										<xsl:with-param name="PN" select="1"/>
									</xsl:call-template>
									<xsl:call-template name="IVB">
										<xsl:with-param name="mot" select="substring($mot,$position + 1,string-length($mot)-$position +1)"/>
										<xsl:with-param name="position" select="1"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:choose>
										<xsl:when test="contains(translate(substring($mot,1,$position),$l_maj,$l_maj_A),'A')"><!-- il y avait des majuscules avant -->
											<xsl:call-template name="majuscule">
												<xsl:with-param name="mot" select="substring($mot,$position, string-length($mot)-$position +1)" />
												<xsl:with-param name="prefixe" select="1" />
												<xsl:with-param name="position" select="1" />
												<xsl:with-param name="PN" select="1"/>
											</xsl:call-template>
										</xsl:when>
										<xsl:otherwise>
											<xsl:call-template name="majuscule">
												<xsl:with-param name="mot" select="substring($mot,$position, string-length($mot)-$position +1)" />
												<xsl:with-param name="prefixe" select="0" />
												<xsl:with-param name="position" select="1" />
												<xsl:with-param name="PN" select="1"/>
											</xsl:call-template>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
							<!-- espaces -->
							<xsl:call-template name="espace"/>
						</xsl:when>
						<!-- il n'y a que des chiffres et le mot est terminé -->
						<xsl:when test="$position = string-length($mot)"> <!--or number(translate(substring-after($mot,$position),',0&sup2;&sup3;','.111'))"> conversion des , en . et des 0, exp2 et 3 en 1 car number(0) et number(NNN,NNN) renvoie faux -->
							<xsl:call-template name="numerique">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="position" select="string-length($mot) + 1" />
								<xsl:with-param name="positionPrec" select="1" />
								<xsl:with-param name="PNS" select="1"/>
								<xsl:with-param name="PNActif" select="$PNActif"/>
							</xsl:call-template>
						</xsl:when>
						<!-- il n'y a que des chiffres pour l'instant -->
						<xsl:otherwise>
							<xsl:call-template name="numerique">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="position" select="$position + 1" />
								<xsl:with-param name="PNS" select="1"/>
								<xsl:with-param name="PNActif" select="$PNActif"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
				<!-- le contenu n'est pas uniquement numérique ou pas numérique-->
					<xsl:variable name="precNum" as="xs:boolean" select="fn:matches(preceding-sibling::*[1],'^(\+|-)?\d+((\.|,)\d+)?$')"/>
					<xsl:choose>
						<!-- y avait-il un contenu numérique avant? -->
						<xsl:when test="$PNS = 1 and not($precNum)"><!-- oui -->
							<xsl:if test="not($PNActif = 1 or $precNum)">
								<xsl:text>&pt6;</xsl:text>
							</xsl:if>
							<xsl:choose>
								<!-- faut-il utiliser l'indicateur de valeur de base pour la suite? -->
								<xsl:when test="contains(substring(translate($mot,$l_amb,$l_amb_ahat),$position,string-length($mot)-$position +1),'â')">
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="substring($mot,1,$position - 1)" />
										<xsl:with-param name="prefixe" select="0" />
										<xsl:with-param name="position" select="1" />
										<xsl:with-param name="PN" select="1"/>
									</xsl:call-template>
									<xsl:call-template name="IVB">
										<xsl:with-param name="mot" select="substring($mot,$position,string-length($mot)-$position +1)"/>
										<xsl:with-param name="position" select="1"/>
									</xsl:call-template>
								</xsl:when>

								<xsl:otherwise>
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="prefixe" select="0" />
										<xsl:with-param name="position" select="1" />
										<xsl:with-param name="PN" select="1"/>
									</xsl:call-template>
									<!--espaces-->
									<xsl:call-template name="espace"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<!-- il n'y avait pas de contenu numérique avant -->
							<xsl:choose>
								<!-- il peut y avoir des ambiguités -->
								<xsl:when test="contains(substring(translate($mot,$l_amb,$l_amb_ahat),1,$position),'â')">
									<xsl:call-template name="IVB">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="position" select="$position + 1" />
										<xsl:with-param name="PN" select="0"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="numerique">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="position" select="$position + 1" />
										<xsl:with-param name="PNS" select="0"/>
										<xsl:with-param name="PNActif" select="$PNActif"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<!-- on a lu le mot et il ne contient que des chiffres -->
		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="$PNS=1">
					<xsl:variable name="motSuivNum" select="translate(following-sibling::*[1],',0&sup2;&sup3;','.111')" as="xs:string?"/>
					<xsl:choose>
						<xsl:when test="not($PNActif = 1 or fn:matches(preceding-sibling::*[1],'^(\+|-)?\d+((\.|,)\d+)?$'))">
							<xsl:text>&pt6;</xsl:text>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="1"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="$PNActif"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:choose>
						<!-- on vérifie que le mot suivant ne commence pas par 3 nombres (cas d'un séparateur de milier + unités) OU n'est pas un nombre et que le mot ne contient pas des signes d'opération)-->
						<xsl:when test="local-name(following-sibling::*[1])='mot' and (fn:matches(following-sibling::*[1],'^[0-9]{3}.*') or number($motSuivNum)) and number(translate($mot,',0&sup2;&sup3;','.111'))"><!-- conversion des , en . et des 0 en 1 car number(0) et number(NNN,NNN) renvoie faux -->
							<!--
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="prefixe" select="0" />
								<xsl:with-param name="position" select="1" />
							</xsl:call-template>-->
							
							<xsl:text>&pt3;</xsl:text>
						</xsl:when>
						<!-- le mot précédent n'est pas composé uniquement de 0123456789 -->
						
							<!-- espaces -->
							<!-- le suivant n'est pa sun nombre et lui-mème n'en est pas un -->
							<!--<xsl:if test="not(number($motSuivNum) and number(translate($mot,',0&sup2;&sup3;','.111')))">
								<xsl:call-template name="espace"/>
							</xsl:if>-->
							<xsl:otherwise>
								<xsl:call-template name="espace"/>
							</xsl:otherwise>
						</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="majuscule">
						<xsl:with-param name="mot" select="$mot" />
						<xsl:with-param name="prefixe" select="0" />
						<xsl:with-param name="position" select="1" />
					</xsl:call-template>
					<!-- espaces-->
					<xsl:call-template name="espace"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="IVB">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="position" as="xs:integer" select='0'/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:choose>
		<xsl:when test="$position = string-length($mot)">
			<xsl:if test="contains(translate($mot,$l_amb,$l_amb_ahat),'â')">
				<xsl:text>&pt56;</xsl:text>
				<xsl:call-template name="majuscule">
					<xsl:with-param name="mot" select="$mot" />
					<xsl:with-param name="prefixe" select="0" />
					<xsl:with-param name="position" select="1" />
					<xsl:with-param name="PN" select="0"/>
				</xsl:call-template>
				<xsl:call-template name="espace"/>
			</xsl:if>
		</xsl:when>
		<!-- on cherche le contenu numérique -->
		<xsl:when test="contains($l_num,substring($mot,$position,1))">
			<xsl:choose>
				<xsl:when  test="contains(substring(translate($mot,$l_amb,$l_amb_ahat),1,$position - 1),'â')">
					<xsl:if test="not($PN = 0)">
						<xsl:text>&pt56;</xsl:text>
					</xsl:if>
					<xsl:call-template name="majuscule">
						<xsl:with-param name="mot" select="substring($mot,1,$position - 1)" />
						<xsl:with-param name="prefixe" select="0" />
						<xsl:with-param name="position" select="1" />
						<xsl:with-param name="PN" select="0"/>
					</xsl:call-template>
					<xsl:call-template name="numerique">
						<xsl:with-param name="mot" select="substring($mot,$position,string-length($mot) - $position + 1)" />
						<xsl:with-param name="position" select="1" />
						<xsl:with-param name="positionPrec" select="0" />
						<xsl:with-param name="PNS" select="3" />
						<xsl:with-param name="PNActif" select="0"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="majuscule">
						<xsl:with-param name="mot" select="substring($mot,1,$position - 1)" />
						<xsl:with-param name="prefixe" select="0" />
						<xsl:with-param name="position" select="1" />
						<xsl:with-param name="PN" select="1"/>
					</xsl:call-template>
					<xsl:if test="not($position = string-length($mot))">
						<xsl:call-template name="numerique">
							<xsl:with-param name="mot" select="substring($mot,$position,string-length($mot) - $position + 1)" />
							<xsl:with-param name="position" select="1" />
							<xsl:with-param name="positionPrec" select="0" />
							<xsl:with-param name="PNS" select="3" />
							<xsl:with-param name="PNActif" select="1"/>
						</xsl:call-template>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="IVB">
				<xsl:with-param name="mot" select="$mot"/>
				<xsl:with-param name="position" select="$position + 1"/>
				<xsl:with-param name="PN" select="$PN"/>
			</xsl:call-template>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- ***************************************************
		MAJUSCULES
	*************************************************
-->

<xsl:template name="majuscule">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="prefixe" as="xs:integer" select='-10'/>
	<xsl:param name="position" as="xs:integer" select='-10'/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:param name="passage" select='1' as="xs:integer"/>
	<xsl:choose>
		<xsl:when test="not(contains(translate($mot,$l_maj,$l_maj_A),'A'))">
			<xsl:call-template name="symbolesComposes">
					<xsl:with-param name="mot" select="$mot"/>
					<xsl:with-param name="PN" select="$PN"/>
			</xsl:call-template>
		</xsl:when>
		<!-- le mot ne contient pas de minuscules ni de chiffres, ni de point (cas d'un sigle) ou bien est potentiellement dans un passage et on prend en compte la règle du passage-->
		<xsl:when test="not(contains(translate($mot,concat('.°&sup2;&sup3;',$l_min),concat('aaaa',$l_min_a)),'a')) and (not($passage = 0) and $cp_part)">
		<!--not(contains($mot,'abcdefghijklmnopqrstuvwxyzz'))">-->
			<xsl:choose>
				<!-- le mot ne contient pas de majuscules-->
				<xsl:when test="not(contains(translate($mot,$l_maj,$l_maj_A),'A'))">
					<xsl:call-template name="symbolesComposes">
							<xsl:with-param name="mot" select="$mot"/>
							<xsl:with-param name="PN" select="$PN"/>
					</xsl:call-template>
				</xsl:when>
				<!-- le mot contient des majuscules mais on n'applique pas de règles complémentaires -
				<xsl:when test="$cp_2 = 0 and $cp_mix = 0">
					<xsl:if test="starts-with(translate($mot,'ABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;','AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'),'A')">
						<xsl:text>&pt46;</xsl:text>
					</xsl:if>
					<xsl:call-template name="symbolesComposes">
						<xsl:with-param name="mot" select="$mot"/>
						<xsl:with-param name="PN" select="$PN"/>
					</xsl:call-template>
				</xsl:when>-->
				<xsl:otherwise>
					<!-- on va utiliser une variable pour savoir si c'est le premier mot en majuscule d'un passage -->
					 <xsl:variable name="position1MotMaj">
						<xsl:call-template name="donnePosition1MotMaj">
							<xsl:with-param name="position" select="1" />
						</xsl:call-template>
					</xsl:variable>
					 <xsl:variable name="positionDMotMaj">
						<xsl:call-template name="donnePositionDMotMaj">
							<xsl:with-param name="position" select="1" />
						</xsl:call-template>
					</xsl:variable>
					<!--<xsl:value-of select="trace(concat($position1MotMaj,' ',$positionDMotMaj),'position 1 / fin:)')"/>-->
					<xsl:choose>
						<!-- le mot est-il dans un passage en majuscule? -->
						<!-- c'est le 1er mot d'un passage en majuscule -->
						<xsl:when test="$position1MotMaj ='1' and $positionDMotMaj = '1'">
						<!-- on n'est pas dans un passage en majuscule -->
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="prefixe" select="$prefixe" />
								<xsl:with-param name="position" select="$position" />
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="passage" select="0"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="$position1MotMaj = '1'">
							<!-- y a-t-il au moins 3 autres mots suivants en majuscules?-->
							<xsl:variable name="premierMotPassageMaj">
								<xsl:call-template name="estSuiviMaj">
									<xsl:with-param name="nb" select="3"/>
									<xsl:with-param name="position" select="1"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="$premierMotPassageMaj = '1'">
									<xsl:text>&pt25;&pt46;</xsl:text>
									<xsl:call-template name="symbolesComposes">
										<xsl:with-param name="mot" select="$mot"/>
										<xsl:with-param name="PN" select="$PN"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="prefixe" select="$prefixe" />
										<xsl:with-param name="position" select="$position" />
										<xsl:with-param name="PN" select="$PN"/>
										<xsl:with-param name="passage" select="0"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:when test="$positionDMotMaj = '1'">
						<!-- dernier mot -->
							<xsl:variable name="dernierMotPassageMaj">
								<xsl:call-template name="estPrecedeMaj">
									<xsl:with-param name="nb" select="3"/>
									<xsl:with-param name="position" select="1"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="$dernierMotPassageMaj = '1'">
									<xsl:text>&pt46;</xsl:text>
									<xsl:call-template name="symbolesComposes">
										<xsl:with-param name="mot" select="$mot"/>
										<xsl:with-param name="PN" select="$PN"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="prefixe" select="$prefixe" />
										<xsl:with-param name="position" select="$position" />
										<xsl:with-param name="PN" select="$PN"/>
										<xsl:with-param name="passage" select="0"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<!--
						<xsl:when test="$position1MotMaj = '3'">
							<xsl:text>2ème</xsl:text>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
						</xsl:when>-->
						<xsl:otherwise>
							<!-- ni le premier ni le dernier, est-ce un passage? -->
							<xsl:variable name="nbSuivant">
								<xsl:call-template name="estSuiviMaj">
									<xsl:with-param name="nb" select="2"/>
									<xsl:with-param name="position" select="1"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:variable name="nbPrec">
								<xsl:call-template name="estPrecedeMaj">
									<xsl:with-param name="nb" select="2"/>
									<xsl:with-param name="position" select="1"/>
								</xsl:call-template>
							</xsl:variable>
							<xsl:choose>
								<xsl:when test="$nbSuivant + $nbPrec = 0">
								<!-- passage pas en majuscule -->
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="$mot" />
										<xsl:with-param name="prefixe" select="$prefixe" />
										<xsl:with-param name="position" select="$position" />
										<xsl:with-param name="PN" select="$PN"/>
										<xsl:with-param name="passage" select="0"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>	
									<xsl:call-template name="symbolesComposes">
										<xsl:with-param name="mot" select="$mot"/>
										<xsl:with-param name="PN" select="$PN"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<!-- traitement d'un mot contenant des majuscules mais pas dans un passage -->
			<xsl:choose>
				<!-- le mot contient des majuscules mais on n'applique pas de règles complémentaires -->
				<xsl:when test="not($cp_2) and not($cp_mix)">
					<xsl:if test="starts-with(translate($mot,$l_maj,$l_maj_A),'A')">
						<xsl:text>&pt46;</xsl:text>
					</xsl:if>
					<!-- cas des mots en majuscules avec trait d'union ou apostrophe-->
					<xsl:choose>
						<!-- le mot est entièrement en majusucle et contient au moins un trait d'union -->
						<xsl:when test="(contains($mot,'-') or contains($mot,'&#8209;')) and not(contains(translate($mot,concat('.°',$l_min),concat('aa',$l_min_a)),'a'))">
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="substring-before($mot,'-')"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
							<xsl:text>&pt36;</xsl:text>
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="substring-after($mot,'-')"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="(contains($mot,$apos) or contains ($mot,'’')) and not(contains(translate($mot,concat('.°',$l_min),concat('aa',$l_min_a)),'a'))">
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="functx:substring-before-match($mot,concat('[',$apos,'’]'))"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
							<xsl:text>&pt3;</xsl:text>
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="functx:substring-after-match($mot,concat('[',$apos,'’]'))"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
						</xsl:when>						
						<xsl:otherwise>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="not(contains(translate($mot,concat('.°',$l_min),concat('aa',$l_min_a)),'a'))">
				<!-- le mot est entièrement en majuscule et contient au minimum une majuscule-->
					<xsl:choose>
						<!-- le mot commence par un numérique -->
						<xsl:when test="contains(translate(substring($mot, 1, 1), '0123456789', '1111111111'),'1')">
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="substring($mot, 1, 1)"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
							<xsl:choose>
								<!-- doit-on appliquer les règles de contenu mixte -->
								<xsl:when test="$cp_mix">
									<xsl:call-template name="majuscule">
										<xsl:with-param name="mot" select="substring($mot, 2, string-length($mot) - 1)" />
										<xsl:with-param name="prefixe" select="0" />
										<xsl:with-param name="position" select="1" />
										<xsl:with-param name="PN" select="$PN"/>
										<xsl:with-param name="passage" select="0"/>
									</xsl:call-template>
								</xsl:when>
								<xsl:otherwise>
									<xsl:call-template name="symbolesComposes">
										<xsl:with-param name="mot" select="substring($mot, 2, string-length($mot) - 1)"/>
										<xsl:with-param name="PN" select="$PN"/>
									</xsl:call-template>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="contains(translate($mot,$l_maj,$l_maj_A),'A') and not($prefixe= 1) 
								and not(preceding-sibling::mot[1][@doSpace='false'] and upper-case(preceding-sibling::mot[1]) = preceding-sibling::mot[1]) ">
								<xsl:if test="(not(string-length(translate($mot, $l_maj,'')) = string-length($mot) - 1) or
									(@doSpace='false' and upper-case(following-sibling::mot[1]) = following-sibling::mot[1])) and $cp_2"><!-- il y a plus d'une majuscule-->
									<!--<xsl:value-of select="translate($mot, $l_maj,'')"/>-->
									<xsl:text>&pt46;</xsl:text>
								</xsl:if>
								<xsl:text>&pt46;</xsl:text>
							</xsl:if>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:when test="$position > string-length($mot)">
					<!-- c'est fini: on fait rien -->
					<!--<xsl:if test="$prefixe = 0">
						<xsl:text>&pt46;</xsl:text>
						<xsl:if test="string-length(.) > 1">
							<xsl:text>&pt46;</xsl:text>
						</xsl:if>
						<xsl:call-template name="symbolesComposes">
							<xsl:with-param name="mot" select="."/>
							<xsl:with-param name="PN" select="$PN"/>
						</xsl:call-template>
					</xsl:if>-->
				</xsl:when>
				<xsl:when test="$prefixe = 1">
					<xsl:if test="contains($l_maj,substring($mot,$position,1))">
						<xsl:text>&pt46;</xsl:text>
					</xsl:if>
					<xsl:choose>
						<xsl:when test="($position= 1 or $position=string-length($mot)) and substring($mot,$position,1)='-'">
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="substring($mot,$position,1)"/>
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="tiret" select='2'/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="substring($mot,$position,1)"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:call-template name="majuscule">
						<xsl:with-param name="mot" select="$mot" />
						<xsl:with-param name="prefixe" select="1" />
						<xsl:with-param name="position" select="$position + 1" />
						<xsl:with-param name="PN" select="$PN"/>
						<xsl:with-param name="passage" select="0"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="not(contains(translate(substring($mot,$position,1),concat('°.',$l_min),concat('aa',$l_min_a)),'a'))">
					<xsl:call-template name="majuscule">
						<xsl:with-param name="mot" select="$mot" />
						<xsl:with-param name="prefixe" select="0" />
						<xsl:with-param name="position" select="$position + 1" />
						<xsl:with-param name="PN" select="$PN"/>
						<xsl:with-param name="passage" select="0"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="not($cp_mix)">
					<xsl:if test="starts-with(translate($mot,$l_maj,$l_maj_A),'A')">
						<xsl:text>&pt46;</xsl:text>
					</xsl:if>
					<xsl:call-template name="symbolesComposes">
						<xsl:with-param name="mot" select="$mot"/>
						<xsl:with-param name="PN" select="$PN"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
		<!---->
					<!-- on recommence au début du mot et cette fois on préfixe toutes les majuscules -->
		<!---->
					<xsl:choose>
						<xsl:when test="contains($mot,'-') and not(starts-with($mot,'-')) and not(substring($mot,string-length($mot),1)='-') and $cp_mix">
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="substring-before($mot, '-')" />
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="position" select="1" />
								<xsl:with-param name="passage" select="0"/>
								<xsl:with-param name="prefixe" select="0" />
							</xsl:call-template>
							<xsl:text>&pt36;</xsl:text>
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="substring-after($mot, '-')" />
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="passage" select="0"/>
								<xsl:with-param name="position" select="1" />
								<xsl:with-param name="prefixe" select="0" />
							</xsl:call-template>
						</xsl:when>
						<!-- le mot commence par une majuscule, et le reste est en minuscule -->
						<xsl:when test="substring($mot,2,string-length($mot)-1) = fn:lower-case(substring($mot,2,string-length($mot)-1)) ">
							<xsl:text>&pt46;</xsl:text>
							<xsl:call-template name="symbolesComposes">
								<xsl:with-param name="mot" select="$mot"/>
								<xsl:with-param name="PN" select="$PN"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:when test="$position + 1 &lt; string-length($mot)">
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="substring($mot, 1, $position)" />
								<xsl:with-param name="prefixe" select="1" />
								<xsl:with-param name="position" select="1" />
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="passage" select="0"/>
							</xsl:call-template>
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="substring($mot, $position + 1, string-length($mot) - $position)" />
								<xsl:with-param name="prefixe" select="0" />
								<xsl:with-param name="position" select="1" />
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="passage" select="0"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:call-template name="majuscule">
								<xsl:with-param name="mot" select="$mot" />
								<xsl:with-param name="prefixe" select="1" />
								<xsl:with-param name="position" select="1" />
								<xsl:with-param name="PN" select="$PN"/>
								<xsl:with-param name="passage" select="0"/>
							</xsl:call-template>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="finBijection">
	<xsl:param name="mot" as="xs:string"/>
	<xsl:param name="PN" as="xs:integer" select='-10'/>
	<xsl:choose>
		<!-- ajout de règles de coupure -->
		<xsl:when test="contains($mot, '@')">
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="substring-before($mot,'@')"/>
			</xsl:call-template>
			<xsl:text>&pt345;</xsl:text><!--<xsl:value-of select='$coupe'/>-->
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="substring-after($mot,'@')"/>
			</xsl:call-template>			
		</xsl:when>
		<xsl:when test="contains($mot, '/')">
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="substring-before($mot,'/')"/>
			</xsl:call-template>
			<xsl:text>&pt34;</xsl:text><!--<xsl:value-of select='$coupe'/>-->
			<xsl:call-template name="bijection">
				<xsl:with-param name="mot" select="substring-after($mot,'/')"/>
			</xsl:call-template>
		</xsl:when>
		<xsl:when test='$mot="&apos;"'>
			<xsl:text><!--&pt2356;-->&pt3;</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<!-- utilisation d'une variable (pb des '")-->
			<!-- NEW XSL2 : '' ou "" permet d'obtenir le symbole choisi sans pb -->
			<xsl:variable name="tousLesSymboles" as="xs:string">
				<xsl:value-of select="concat('ÁÍÓÚÑÌÒÄÖáíóúñìòäöABCDEFGHIJKLMNOPQRSTUVWXYZÀÂÉÈÊËÎÏÔÙÛÜ&Ccedil;&AElig;&OElig;abcdefghijklmnopqrstuvwxyz0123456789àâéèêëîïôùûüçæœ@+-×÷=.,&nbsp;/&#8209;’‘:',$apos)"/>
			</xsl:variable>
			
			<xsl:variable name="v2">
				<!-- <xsl:choose>
					là, c'est vraiment se casser la tête pour pas grand chose, c'est vrai... 
					Fred : d'ailleurs je le mets en commentaire pour optimisation
					<xsl:when test="ancestor::*[@lang='es']">
						<xsl:text>&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12356;&pt16;&pt2346;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt2346;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt256;&pt2;&pt;&pt34;&pt36;&pt3;&pt3;&pt25;&pt3;</xsl:text>
						: &#8209; = trait d'union insécable 
					</xsl:when>
					<xsl:otherwise> -->
						<xsl:text>&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt12356;&pt34;&pt346;&pt23456;&pt12456;&pt34;&pt346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt1;&pt12;&pt14;&pt145;&pt15;&pt124;&pt1245;&pt125;&pt24;&pt245;&pt13;&pt123;&pt134;&pt1345;&pt135;&pt1234;&pt12345;&pt1235;&pt234;&pt2345;&pt136;&pt1236;&pt2456;&pt1346;&pt13456;&pt1356;&pt3456;&pt16;&pt126;&pt146;&pt1456;&pt156;&pt1246;&pt12456;&pt1256;&pt246;&pt12356;&pt16;&pt123456;&pt2346;&pt126;&pt1246;&pt146;&pt12456;&pt1456;&pt23456;&pt156;&pt1256;&pt12346;&pt345;&pt246;&pt345;&pt235;&pt36;&pt35;&pt25;&pt2356;&pt256;&pt2;&pt;&pt34;&pt36;&pt3;&pt3;&pt25;&pt3;</xsl:text>
						<!-- ne rien mettre pour &nbsp-->
					<!-- </xsl:otherwise>
				</xsl:choose> -->
			</xsl:variable>
			<xsl:value-of select="translate($mot,$tousLesSymboles,$v2)" />
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>
</xsl:stylesheet>