/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret, Frédérick Schwebel, Vivien Guillet
 * Contact: natbraille@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

package ui;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import nat.ConfigNat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


/**
 * Onglet de configuration de la mise en page avancée
 * @see OngletConf
 * @since 2.0
 * @author bruno
 *
 */
public class ConfMiseEnPageAvancee extends OngletConf implements ChangeListener, ActionListener
{
	/** pour la sérialisation (non utilisé) */
	private static final long serialVersionUID = 1L;
	
	/** ArrayList des JSpinner pour les titres*/
	private ArrayList<JSpinner> jsTitres = new ArrayList<JSpinner>();
	/** ArrayList pour les JLabel de jsTitres
	 * @see ConfMiseEnPageAvancee#jsTitres
	 */
	private ArrayList<JLabel> jlTitres = new ArrayList<JLabel>();
	/** Variable indiquant s'il y a eu des changements sur les titres */
	private boolean modifTitre = false;
	/** Constante représentannt les niveaux de titres stricts à appliquer */
	private static final String TITRES_STRICTS = "1,2,3,4,5,5,5,5,5";
	/** La table pour l'ajout des caractères de formatage spécifiques*/
	private JTable jtableAjout;
	/** Tableau pour les données de la table */
	private Object[][] donnees;
	/** Activer de manière stricte les styles braille tels que définis dans la norme */
	private JRadioButton jrbTitresStrict = new JRadioButton("Application stricte des styles Braille");
	/** Activer le réglage prsonalisé des styles braille  */
	private JRadioButton jrbTitresPerso = new JRadioButton("Réglage personnalisé des niveaux de titre");
	/** Label de titre intérieur de l'onglet */
	private JLabel titre;
	/** Label de sous-titre pour niveaux de titres */
	private JLabel titre2;
	/** Label de sous-titre pour chaînes à ajouter */
	private JLabel titre3;
	/** Label de sous-titre pour chaîne à remplacer */
	private JLabel titre4;
	/** Champ de saisie de la chaine d'entrée à remplacer si no mep */
	private JTextField jtfChaineEntree = new JTextField(5);
	/** Champ de saisie de la chaine qui remplace celle d'entrée si no mep */
	private JTextField jtfChaineSortie = new JTextField(5);
	/** Label de la chaine d'entrée */
	private JLabel jlChaineIn = new JLabel ("Remplacer ");
	/** Label de la chaine de sortie */
	private JLabel jlChaineOut = new JLabel (" par ");
	
	/**
	 * Constructeur de l'onglet Mise en page avancée
	 *
	 */
	public ConfMiseEnPageAvancee()
	{
		super();
		getAccessibleContext().setAccessibleDescription("Activez cet onglet pour afficher les options de mise en page avancée");
		getAccessibleContext().setAccessibleName("Onglet contenant les options de mise en page avancée");

		/**********
		 * Préparation des composants
		 */
		
		/* Titres */
		jrbTitresStrict.getAccessibleContext().setAccessibleName("Case à cocher application stricte des titres brailles");
		jrbTitresStrict.getAccessibleContext().setAccessibleDescription("Sélectionner cette option pour utiliser les titres brailles de manière stricte");
		jrbTitresStrict.setToolTipText("Sélectionner cette option pour utiliser les titres brailles de manière stricte (Alt+c)");
		jrbTitresStrict.setMnemonic('c');
		
		jrbTitresPerso.getAccessibleContext().setAccessibleName("Case à cocher application personnalisée des titres brailles");
		jrbTitresPerso.getAccessibleContext().setAccessibleDescription("Sélectionner cette option pour utiliser les titres brailles de manière personnalisée");
		jrbTitresPerso.setToolTipText("Sélectionner cette option pour utiliser les titres brailles de manière personnalisée (Alt+r)");
		jrbTitresPerso.setMnemonic('r');
		
		ButtonGroup grpb = new ButtonGroup();
		grpb.add(jrbTitresStrict);
		grpb.add(jrbTitresPerso);
		jrbTitresPerso.setSelected(!ConfigNat.getCurrentConfig().getTitresStricts());
		jrbTitresPerso.addActionListener(this);
		jrbTitresStrict.setSelected(ConfigNat.getCurrentConfig().getTitresStricts());
		jrbTitresStrict.addActionListener(this);
		
		String[] listeTitre = ConfigNat.getCurrentConfig().getNiveauxTitres().split(",");
		int[] listeT = new int[listeTitre.length];
		
		for(int i=0; i<listeTitre.length;i++){listeT[i]=Integer.parseInt(listeTitre[i]);}
		
		//for(int i=0;i<listeT.length;i++)
		for(int i=0;i<6;i++)
		{
			jlTitres.add(new JLabel("Un titre de niveau "+ (i+1) + " en noir donne un titre braille de niveau "));
			jlTitres.get(i).setDisplayedMnemonic(49 + i);//48 + 1 + i représente le code ascci du char représentant i
			jsTitres.add(new JSpinner(new SpinnerNumberModel(listeT[i], 1, 5, 1)));
			jlTitres.get(i).setLabelFor(jsTitres.get(i));
			jsTitres.get(i).getAccessibleContext().setAccessibleName("Liste déroulante niveau de titre braille ("+(i+1)+")");
			jsTitres.get(i).getAccessibleContext().setAccessibleDescription("Sélectionner avec les flèches le niveau de titre braille ("+(i+1)+")");
			jsTitres.get(i).setToolTipText("niveau du titre braille ("+(i+1)+")");
			jsTitres.get(i).addChangeListener(this);
		}
	    
		/*
		 * Chaine à remplacer
		 */
		
		jtfChaineEntree.setText(ConfigNat.getCurrentConfig().getChaineIn());
		jtfChaineSortie.setText(ConfigNat.getCurrentConfig().getChaineOut());
		
		/*
		 * Fabrication de la table ajout
		 */
		donnees = chargeTableAjout();
		TableModel tm = new TableModeleAjout(donnees);
		
		jtableAjout = new JTable(tm);
		jtableAjout.getAccessibleContext().setAccessibleName("Table ajout de caractères");
		jtableAjout.getAccessibleContext().setAccessibleDescription("Table contenant 3 colonnes: avant, " +
			"élément considéré, compter le rajout après dans la longueur de la ligne");
		jtableAjout.setToolTipText("Entrez les chaines à ajouter (Alt+a)");

		//table.setAutoCreateRowSorter(true);
		TableColumnModel modelesColonnes = jtableAjout.getColumnModel();
        TableColumn modelColonne = modelesColonnes.getColumn(0);
        modelColonne.setMaxWidth(100);
        modelColonne.setMinWidth(60);
        
        //modelColonne = modelesColonnes.getColumn(1);
        //modelColonne.setMaxWidth(60);
        //modelColonne.setMinWidth(60);
        
        //modelColonne = modelesColonnes.getColumn(2);
        modelColonne = modelesColonnes.getColumn(1);
        modelColonne.setMaxWidth(150);
        modelColonne.setMinWidth(100);
        
        //modelColonne =  modelesColonnes.getColumn(3);
        modelColonne =  modelesColonnes.getColumn(2);
        modelColonne.setMaxWidth(100);
        modelColonne.setMinWidth(60);
        
        //modelColonne =  modelesColonnes.getColumn(4);
        //modelColonne.setMaxWidth(60);
        //modelColonne.setMinWidth(60);
        
		/*
		 * Mise en page
		 */
		
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(3,3,3,3);
		GridBagLayout gbl = new GridBagLayout();
		JPanel panTr = new JPanel();
		panTr.setLayout(gbl);
		
		titre = new JLabel("<html><h3>Paramétrage avancé</h3></html>");	
		
		gbc.anchor = GridBagConstraints.WEST;
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth=3;
		gbl.setConstraints(titre, gbc);
		panTr.add(titre);
		
		titre2 = new JLabel("Initialisation...");
		gbc.gridwidth = 2;
		gbc.gridy++;
		gbl.setConstraints(titre2, gbc);
		panTr.add(titre2);
		
		gbc.gridy++;
		gbc.gridx=1;
		gbc.gridwidth=3;
		gbl.setConstraints(jrbTitresStrict, gbc);
		panTr.add(jrbTitresStrict);
		
		gbc.gridy++;
		gbl.setConstraints(jrbTitresPerso, gbc);
		panTr.add(jrbTitresPerso);
		
		for(int i=0;i<jsTitres.size();i++)
		{
			gbc.gridy++;
			gbc.gridx=1;
			gbc.gridwidth=2;
			gbl.setConstraints(jlTitres.get(i), gbc);
			panTr.add(jlTitres.get(i));
			
			gbc.gridx=gbc.gridx+2;
			gbc.gridwidth=1;
			gbl.setConstraints(jsTitres.get(i), gbc);
			panTr.add(jsTitres.get(i));
		}
		/*
		 * Chaîne à remplacer
		 */
		titre4 = new JLabel("Initialisation...");
		gbc.gridwidth = 4;
		gbc.gridx=0;
		gbc.gridy++;
		gbl.setConstraints(titre4, gbc);
		panTr.add(titre4);
		
		jlChaineIn.setLabelFor(jtfChaineEntree);
		jlChaineOut.setLabelFor(jtfChaineSortie);
		
		gbc.gridwidth = 1;
		gbc.gridx=0;
		gbc.gridy++;
		//gbc.gridx++;
		gbl.setConstraints(jlChaineIn, gbc);
		panTr.add(jlChaineIn);
		gbc.gridx++;
		gbl.setConstraints(jtfChaineEntree, gbc);
		panTr.add(jtfChaineEntree);
		gbc.gridx++;
		gbl.setConstraints(jlChaineOut, gbc);
		panTr.add(jlChaineOut);
		gbc.gridx++;
		gbl.setConstraints(jtfChaineSortie, gbc);
		panTr.add(jtfChaineSortie);
		
		/*
		 * Table d'ajout des caractères
		 */
		titre3 = new JLabel("Initialisation...");

		titre3.setLabelFor(jtableAjout);
		titre3.setDisplayedMnemonic('a');
		gbc.gridwidth = 4;
		gbc.gridx=0;
		gbc.gridy++;
		gbl.setConstraints(titre3, gbc);
		panTr.add(titre3);
		
		gbc.gridy++;
		gbc.insets = new Insets(0,0,0,0);
		gbc.gridx=1;
		gbl.setConstraints(jtableAjout.getTableHeader(), gbc);
		panTr.add(jtableAjout.getTableHeader());
		
		gbc.gridy++;
		gbl.setConstraints(jtableAjout, gbc);
		panTr.add(jtableAjout);
		
		// TODO à virer
		//JLabel vireMoi = new JLabel("<html><font color=\"red\">Les colonnes \"Compter\" ne servent pas pour l'instant</font></html>");
		//gbc.gridy++;
		//gbl.setConstraints(vireMoi, gbc);
		//panTr.add(vireMoi);
		
		enableTabComponents (ConfigNat.getCurrentConfig().getMep());
		
		setLayout(new BorderLayout());
		add(panTr, BorderLayout.NORTH);

		//bug de sun pour la maj de la table
		jtableAjout.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
	}	
	


  

	/**
	 *  Initialise la table pour les ajouts
	 * @return la double table contenant les rajouts
	 * @see ConfMiseEnPageAvancee#jtableAjout
	 */
	private Object[][] chargeTableAjout()
	{
		String [] listeAjout = {"document","paragraphe","ligne","mathématique","littéraire","musique"};
		//Object[][] retour = new Object[listeAjout.length][5];
		Object[][] retour = new Object[listeAjout.length][3];
		//initialisation
		String raj = ConfigNat.getCurrentConfig().getRajout();
		//		String[] rajouts = raj.split(",");

		String[] rajouts = ConfigNat.intelliSplit(raj,",");

		//String cpt = ConfigNat.getCurrentConfig().getRajoutCompte();
		//String [] comptes = cpt.split(",");
		for(int i=0;i<listeAjout.length;i++)
		{
			retour[i][0] = rajouts[2*i].substring(1,rajouts[2*i].length()-1 );
			//retour[i][1] = new Boolean(comptes[2*i]);
			retour[i][1] = listeAjout[i];
			retour[i][2] = rajouts[2*i+1].substring(1,rajouts[2*i+1].length()-1 );
			//retour[i][4] = new Boolean(comptes[2*i+1]);
		}
		
		return retour;
	}



	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer(java.lang.String)
	 */
	public boolean enregistrer(String f)
	{
		ConfigNat.getCurrentConfig().setFichierConf(f);
		return enregistrer();
	}
	/**
	 * Enregistre les options de l'onglet
	 * @see ui.SavableTabbedConfigurationPane#enregistrer()
	 */
	public boolean enregistrer()
	{
		boolean retour = true;
		/*
		 * les titres
		 */
		ConfigNat.getCurrentConfig().setTitresStricts(jrbTitresStrict.isSelected());
		if(jrbTitresStrict.isSelected())
		{
			ConfigNat.getCurrentConfig().setNiveauxTitres(TITRES_STRICTS);
		}
		else
		{
			boolean saisieOK = true;
			//premier niveau
			int mini = ((Integer)(jsTitres.get(0).getValue())).intValue();
			String titres="" + mini;
			//niveaux suivants
			for(int i=1; i<jsTitres.size();i++)
			{
				int valeur = ((Integer)(jsTitres.get(i).getValue())).intValue();
				titres = titres + "," + valeur;
				saisieOK = saisieOK && (mini<=valeur);
				mini=valeur;
			}
			int rep=JOptionPane.YES_OPTION;
			if(!saisieOK && modifTitre)//la saisie n'est pas bonne ET il y a eu des changements
			{
				rep = JOptionPane.showConfirmDialog(
					    this,
					    "Les niveaux de titres en braille ne sont pas ordonnés comme les niveaux de titre en noir. Voulez-vous continuer?",
					    "Avertissement",
					    JOptionPane.YES_NO_OPTION);
			}
			if(rep==JOptionPane.YES_OPTION)
			{
				ConfigNat.getCurrentConfig().setNiveauxTitres(titres);
			}
		}
		/*
		 * la chaine à remplacer
		 */
		ConfigNat.getCurrentConfig().setChaineIn(jtfChaineEntree.getText());
		ConfigNat.getCurrentConfig().setChaineOut(jtfChaineSortie.getText());
		/*
		 * Les rajouts
		 */
		String ajouts ="";
		//String comptes="";
		for(int i=0;i<donnees.length;i++)
		{
		    String before = (String) donnees[i][0];
		    String after  = (String) donnees[i][2];
		    before = before.replaceAll(",",",,");
		    after = after.replaceAll(",",",,");	   
		    ajouts = ajouts+"'"+before+"','"+after+"',";
		    //comptes= comptes + donnees[i][1]+","+donnees[i][4]+",";
		}
		//Suppression du dernier séparateur
		ajouts = ajouts.substring(0,ajouts.length()-1);
		//comptes = comptes.substring(0,comptes.length()-1);
		
		ConfigNat.getCurrentConfig().setRajout(ajouts);
		//ConfigNat.getCurrentConfig().setRajoutCompte(comptes);
		
		ConfigNat.getCurrentConfig().sauvegarder();
		return retour;
	}
	/**
	 * Implémentation de ChangeListener
	 * mets modifTitre à vrai pour indiquer qu'il y a eu un changement dans la configuration des titres
	 * @see ConfMiseEnPageAvancee#modifTitre
	 * @see javax.swing.event.ChangeListener#stateChanged(javax.swing.event.ChangeEvent)
	 */
	public void stateChanged(ChangeEvent arg0) {modifTitre = true;}
	
	/**
	 * Si mise en page : active les composants de titres / désactive les chaînes
	 * Sinon : active les chaînes / désactive les composants de titres
	 * @param MiseEnPage true si MiseEnPage activée
	 */
	public void enableTabComponents (Boolean MiseEnPage)
	{
		titre2.setText((MiseEnPage)?
				"<html><h4>Correspondances niveaux de titres</h4></html>":
				"<html><h4>Niveaux de titres (actif uniquement si mise en page activée)</h4></html>");
		titre2.setEnabled(MiseEnPage);
		titre2.setForeground((MiseEnPage)?Color.DARK_GRAY:Color.GRAY);
		jrbTitresPerso.setEnabled(MiseEnPage);
		jrbTitresStrict.setEnabled(MiseEnPage);
		for(int i=0;i<jsTitres.size();i++)
		{
			jlTitres.get(i).setEnabled(MiseEnPage && jrbTitresPerso.isSelected());
			jsTitres.get(i).setEnabled(MiseEnPage && jrbTitresPerso.isSelected());
		}
		
		titre4.setText((MiseEnPage)?
				"<html><h4>Cha&icirc;ne (braille) à remplacer (quand il n'y a pas de mise en page)</h4></html>":
				"<html><h4>Cha&icirc;ne (braille) à remplacer</h4></html>");
		titre4.setEnabled(!MiseEnPage);
		titre4.setForeground((!MiseEnPage)?Color.DARK_GRAY:Color.GRAY);
		jlChaineIn.setForeground((!MiseEnPage)?Color.DARK_GRAY:Color.GRAY);
		jlChaineOut.setForeground((!MiseEnPage)?Color.DARK_GRAY:Color.GRAY);
		jtfChaineEntree.setEnabled(!MiseEnPage);
		jtfChaineSortie.setEnabled(!MiseEnPage);
		
		titre3.setText((MiseEnPage)?
				"<html><h4>Cha&icirc;nes à <u>a</u>jouter (quand il n'y a pas de mise en page)</h4></html>":
				"<html><h4>Cha&icirc;nes à <u>a</u>jouter</h4></html>");
		titre3.setEnabled(!MiseEnPage);
		titre3.setForeground((!MiseEnPage)?Color.DARK_GRAY:Color.GRAY);
		jtableAjout.setEnabled(!MiseEnPage);
		jtableAjout.setForeground((MiseEnPage)?Color.LIGHT_GRAY:Color.BLACK);
	}
	
	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent evt) {
		if ((evt.getSource()==jrbTitresStrict) || (evt.getSource()==jrbTitresPerso))
		{
			for(int i=0;i<jsTitres.size();i++)
			{
				jlTitres.get(i).setEnabled(jrbTitresPerso.isSelected());
				jsTitres.get(i).setEnabled(jrbTitresPerso.isSelected());
			}
		}
		
	}
	
	/**
	 * Classe interne de modèle de table pour la table d'ajout
	 * @see ConfMiseEnPageAvancee#jtableAjout
	 * @author bruno
	 *
	 */
	private class TableModeleAjout extends AbstractTableModel
	{
		/** Pour la sérialisation (non utilisé) */
		private static final long serialVersionUID = 1L;
		/** La matrice des objets du tableau */
		private Object[][] data;
		/** le tableau des classes associées à chaque colonne */
		//private Class<?>[] colClass = new Class<?>[]{String.class,Boolean.class,String.class,String.class,Boolean.class};
		private Class<?>[] colClass = new Class<?>[]{String.class,String.class,String.class};
		/** le tableau des noms des colonnes */
		private String[] columnNames = new String[]{"avant", "élément","après"};
		/* le tableau des toolType text pour les noms des colonnes /
		private String[] columnToolTipText = new String[]{"chaine à insérer avant l'élément",
				"prendre en compte la longueur de la chaine insérée avant l'élément dans la longueur de la ligne",
				"type d'élément concerné",
				"chaine à insérer après l'élément",
				"prendre en compte la longueur de la chaine insérée après l'élément dans la longueur de la ligne"};
		*/
		/** 
		 * Constructeur
		 * @param o Les objets du tableau
		 */
		public TableModeleAjout(Object[][] o)
		{
			super();
			data=o;
		}
		
		/**
		 * Renvoie le nom de la colonne n° col
		 * @param col Le numéro de la colonne
		 * @return le nom de la colonne
		 */
		@Override
		public String getColumnName(int col) {return columnNames[col].toString();}
		/**
		 * Modifie la valeur de l'objet de la table situé en (row,col)
		 * redéfinition;
		 * @param value la valeur de modification
		 * @param row la ligne
		 * @param col la colonne de l'objet 
		 */
		@Override
		public void setValueAt(Object value, int row, int col) {
	        data[row][col] = value;
	        fireTableCellUpdated(row, col);
	    }

		/**
		 * Implémentation: Donne le nombre de colonnes de la table
		 * @return le nombre de colonnes
		 */
		public int getColumnCount() {return data[0].length;}
		/**
		 * Implémentation: Donne le nombre de lignes de la table
		 * @return le nombre de lignes
		 */
		public int getRowCount() {return data.length;}
		/**
		 * Implémentation: renvoie l'objet situé en (row, col)
		 * @param row la ligne de l'objet
		 * @param col la colonne de l'objet 
		 * @return l'objet
		 */
		public Object getValueAt(int row, int col) {return data[row][col];}
		/**
		 * Implémentation: renvoie vrai si l'objet situé en (row, col) est éditable
		 * @param row la ligne de l'objet
		 * @param col la colonne de l'objet 
		 * @return vrai si l'objet situé en (row, col) est éditable
		 */
		@Override
		public boolean isCellEditable(int row, int col){return true;}
		/**
		 * Redéfinition: renvoie la class des objets de la colonne col
		 * @param col la colonne
		 * @return la classe des objet de la colonne
		 */
		@Override
		public Class<?> getColumnClass(int col) {return colClass[col];}
	}
}
