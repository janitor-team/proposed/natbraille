/*
 * NAT - An universal Translator
 * Copyright (C) 2005 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
package nat.convertisseur;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.io.InputStream;

import outils.TextConverter;

import nat.ConfigNat;
import nat.Nat;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;

import gestionnaires.GestionnaireErreur;
import outils.Path;
import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * <p>Convertit en odt un document doc, rtf, ou tout autre en faisant 
 * appel à l'API d'openoffice via <code>JODConverter</p>
 * <p>Convertit ensuite cet odt au format interne</p>
 * @author Bruno Mascret, Raphaël Mina
 *
 */
public class Convertisseur2ODT extends Convertisseur
{
	/** Constante pour représenter l'OS LINUX*/
	public static final int OS_LINUX = 1;
	/** Constante pour représenter l'OS WINDOWS*/
	public static final int OS_WINDOWS = 2;
	/** Constante pour représenter l'OS MAC*/
	public static final int OS_MAC = 3;
	/** constante pour le nombre d'essais maximum*/
	private static final int MAX_ATTEMPT = 5;

	/** Le port sur lequel lancer openoffice en mode serveur*/
	private int port = 8100;
	/** adresse du script de lancement généré dans cette classe */
	private String script = ConfigNat.getUserTempFolder()+"/scriptOO";
	/** le process lançant openoffice */
	private Process p = null;
    /** Chemin d'accès à OpenOffice sous Windows */
    private String pathOO=null;
	/** ligne de commande à exécuter pour lancer OpenOffice */
	private String ldc = "soffice -headless -accept=\"socket,host=127.0.0.1,port="+port+";urp;\" -nofirststartwizard";
	/** adresse du fichier odt généré */
	private String tmpOdt = ConfigNat.getUserTempFolder()+"/tmp.odt";
    /** boolean testant l'existence d'un process OpenOffice*/
    private boolean openOfficeRunning=false;
    /**Id du processus OpenOffice existant ou crée par NAT*/
    private String pidSoffice="";
	/** le gestionnaire d'erreur */
	private GestionnaireErreur gest;

	/**
	 * Constructeur
	 * @param src adresse du fichier source
	 * @param tgt adresse du fichier cible
	 */
	public Convertisseur2ODT(String src, String tgt)
	{
		super(src, tgt);
	}

	/**
	 * Convertit en odt un document doc, rtf, ou tout autre en faisant appel à l'API d'openoffice
     * Convertit ensuite cet odt au format interne
     * @param g une instance de GestionnaireErreur
     * @return true si la conversion s'est bien passée
	 */
	@Override
	public boolean convertir(GestionnaireErreur g)
	{
		tempsExecution = System.currentTimeMillis();
		gest = g;
		boolean retour = true;
		File inputFile = new File(source);
		File outputFile = new File(tmpOdt);

		gest.afficheMessage("\n** Conversion de " + source + " au format OO avec JODConverter...", Nat.LOG_NORMAL);
		gest.afficheMessage("\n*** Lancement d'openoffice sur le port " + port +"...", Nat.LOG_SILENCIEUX);
		retour = runOO();

		// connect to an OpenOffice.org instance running on port P
		OpenOfficeConnection connection = null;
		int essai = 1;
		boolean ok = false;
		while (retour && essai<= MAX_ATTEMPT && !ok)
		{
			try
			{
				gest.afficheMessage(essai +"...", Nat.LOG_SILENCIEUX);
				connection = new SocketOpenOfficeConnection(port);
				connection.connect();
				ok = true;
			}
			catch (ConnectException e)
			{
				essai++;
				try {
					Thread.sleep(2000);// Delay before retrying to connect
					if(essai==MAX_ATTEMPT)
					{
						Thread.sleep(5000);
						gest.afficheMessage("dernier essai: augmentation du délai d'attente...", Nat.LOG_SILENCIEUX);
					}
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}

		if(!ok)
		{
			if (retour)
			{
				gest.afficheMessage("\nERREUR: impossible de se connecter à openoffice sur le port "+ port, Nat.LOG_SILENCIEUX);
			}
			else {gest.afficheMessage("\nERREUR: OpenOffice n'est pas installé", Nat.LOG_SILENCIEUX);}
			retour = false;
		}
		else
		{
			gest.afficheMessage("ok\n*** conversion en odt...", Nat.LOG_VERBEUX);
			// convert
			DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
			converter.convert(inputFile, outputFile);
			gest.afficheMessage("ok\n*** deconnexion...", Nat.LOG_VERBEUX);
			// close the connection
			connection.disconnect();
			p.destroy();

            killOO();
            gest.afficheMessage("ok\n", Nat.LOG_VERBEUX);

			// utilisation de ConvertisseurOO
			ConvertisseurOpenOffice c = new ConvertisseurOpenOffice(tmpOdt, cible);
			c.convertir(gest);

			tempsExecution = System.currentTimeMillis() - tempsExecution;
		}
		return retour;
	}

	/**
	 * Fabrique le script de lancement d'openoffice en fonction de l'OS
	 * @param os indique le système d'exploitation
	 * @return true si OO a été détecté et que le script a bien été créé
	 */
	public boolean fabriqueExec(int os)
	{
		Boolean exec_ok = true;
		
		if(os == OS_LINUX)
		{
            BufferedReader bufferedReader = null;
            InputStream out = null;

            try {
                /**recherche d'une instance d'openOffice déjà lancé**/
                gest.afficheMessage("\n***Recherche " +
                        "d'OpenOffice.org : ", Nat.LOG_VERBEUX);
                out = Runtime.getRuntime().exec("which soffice").getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(out));
                pathOO = bufferedReader.readLine();
                if (pathOO==null)
                {
                    gest.afficheMessage("\n\n******************************\n" +
                            "OpenOffice.org doit être installé pour que" +
                            " NAT accepte les formats propriétaires. Ce " +
                            "logiciel est disponible gratuitement à l'adresse" +
                            " http://fr.openoffice.org\n" +
                            "******************************\n\n", Nat.LOG_SILENCIEUX);
                    exec_ok = false;
                    	
                }
                else
                {
                    ldc=ldc.replaceFirst("soffice",pathOO);
                    gest.afficheMessage("\n***Système d'exploitation: Linux", Nat.LOG_VERBEUX);
        			gest.afficheMessage("\n***Création du script", Nat.LOG_VERBEUX);
        			try
        			{
        				BufferedWriter fichier = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(script),"UTF-8"));
        				//FileWriter fichierXSL = new FileWriter(filtre);
        				fichier.write("#!/bin/sh\n#Generated file/fichier genere par NAT\n");
        				fichier.write("exec=exec\n");
        				fichier.write("exec " + ldc);
                        Runtime.getRuntime().exec("chmod 755 " + script);
        				fichier.close();
        			}
        			catch (IOException e)
        			{
        				gest.afficheMessage("\nErreur lors de la création du script de lancement d'OOo" + e,Nat.LOG_NORMAL);
        				exec_ok = false;
        			}
                }
            } 
            catch (Exception officeException) 
            {
                officeException.printStackTrace();
                gest.afficheMessage("\nErreur lors de la recherche de "+
                        "OpenOffice.org sur la machine", Nat.LOG_NORMAL);
                exec_ok = false;
            }	
		}
		else if(os == OS_WINDOWS)
		{

            script+=".bat";//ajout de l'extension afin que le script tourne sous Windows
			gest.afficheMessage("\n***Système d'exploitation: Windows", Nat.LOG_VERBEUX);
			gest.afficheMessage("\n***Création du script", Nat.LOG_VERBEUX);
            Path path = new Path(gest);
            pathOO = path.getOOPath();
            if (pathOO.isEmpty())
            {
                gest.afficheMessage("\n\n******************************\n" +
                        "OpenOffice.org doit être installé "+
                        "pour que NAT accepte les formats propriétaires. Ce "+
                        "logiciel est disponible gratuitement à l'adresse "+
                        "http://fr.openoffice.org\n"+
                        "******************************\n\n",Nat.LOG_SILENCIEUX);
                exec_ok = false;
            }
            //gest.afficheMessage(pathOO,Nat.LOG_VERBEUX);
			if (exec_ok) try
			{
				BufferedWriter fichier = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(script),"UTF-8"));
                ldc = ldc.replaceFirst("soffice","\""+pathOO+"\"");
				fichier.write(ldc);
				fichier.close();
			}
			catch (IOException e)
			{
				gest.afficheMessage("\nErreur lors de la création du script de lancement d'OOo" + e,Nat.LOG_NORMAL);
				exec_ok = false;
			}
		}
		else
		{
			gest.afficheMessage("\n** Système d'exploitation non reconnu; arrêt du traitement \n",Nat.LOG_SILENCIEUX);
			exec_ok = false;
		}
		
		return exec_ok;
	}

	/**
	 * Lance openoffice en mode serveur
	 * @return true si c'est fait
	 */
	public boolean runOO()
	{
		boolean OO_Ok = true;
		
		gest.afficheMessage("\nLancement d'OpenOffice en mode serveur", Nat.LOG_NORMAL);
		// setLdc(ConfigNat.getCurrentConfig().getCommande()); maintenant dans FabriqueExec
		//gest.afficheMessage("\nSystème : " + System.getProperty("os.name")+"\n",Nat.LOG_DEBUG);
		if(System.getProperty("os.name").startsWith("Linux"))
		{
            gest.afficheMessage("\n**Ecriture du script", Nat.LOG_VERBEUX);
            OO_Ok = fabriqueExec(OS_LINUX);
            if (OO_Ok)
            {
				gest.afficheMessage("\n**Lancement du script d'OOo", Nat.LOG_VERBEUX);
				Runtime runTime = Runtime.getRuntime();
				int res = 0;
	
	            BufferedReader bufferedReader=null;
	            InputStream out = null;
	
	            try
	            {
	            /**recherche d'une instance d'openOffice déjà lancé**/
	            gest.afficheMessage("\nProcessus "+
	                    "OpenOffice déjà existant : ", Nat.LOG_NORMAL);
	            out = runTime.exec("ps -C soffice.bin -o pid=").getInputStream();
	            bufferedReader = new BufferedReader(new InputStreamReader(out));
	            if ((bufferedReader.readLine())!=null)
	            {
	                openOfficeRunning = true;
	                gest.afficheMessage(" oui\n",Nat.LOG_NORMAL);
	            }
	            else
	            {
	                gest.afficheMessage(" non\n",Nat.LOG_NORMAL);
	            }
	
	
	            }
	            catch (Exception officeException)
	            {
	                gest.afficheMessage("\nErreur de détection d'un processus"+
	                        "OpenOffice existant", Nat.LOG_NORMAL);
	            }
	
	
	            try
				{
					p = runTime.exec(script);
					res = p.waitFor();
	                if (!openOfficeRunning)
	                {
	                    out = runTime.exec("ps -C soffice.bin -o pid=").getInputStream();
	                    bufferedReader = new BufferedReader(new InputStreamReader(out));
	                    pidSoffice = bufferedReader.readLine();
	                }
	
				}
				catch (IOException e) {gest.afficheMessage("\nErreur d'entrée/sortie", Nat.LOG_NORMAL); OO_Ok = false;}
				catch (InterruptedException e) {gest.afficheMessage("\nErreur de communication avec openoffice", Nat.LOG_NORMAL); OO_Ok = false;}
				if (res != 0)
				{//le processus p ne s'est pas terminé normalement
					gest.afficheMessage("\nLe script de lancement d'OOo a renvoyé une erreur", Nat.LOG_NORMAL);
					OO_Ok = false;
				}
            }
		}
		else if(System.getProperty("os.name").startsWith("Windows"))
		{
			Runtime runTime = Runtime.getRuntime();
			int res = 0;
			gest.afficheMessage("\n**Ecriture du script", Nat.LOG_VERBEUX);
			OO_Ok = fabriqueExec(OS_WINDOWS);
			if (OO_Ok)
			{
				gest.afficheMessage("\n**Lancement du script d'OOo", Nat.LOG_VERBEUX);
				TextConverter f = new TextConverter (script);
				try
				{
					f.convert(); //conversion des lf en crlf pour impression
				}
				catch (Exception e) {gest.afficheMessage("\nErreur de TextConverter", Nat.LOG_NORMAL);}
				try
				{
					p = runTime.exec(ldc);
					//res = p.waitFor();
				}
				catch (IOException e) {gest.afficheMessage("\nErreur d'entrée/sortie", Nat.LOG_NORMAL);}
				//catch (InterruptedException e) {gest.afficheMessage("\nErreur de communication avec OOo", Nat.LOG_NORMAL);}
				if (res != 0)
				{//le processus p ne s'est pas terminé normalement
					gest.afficheMessage("\nLe processus de lancement d'OOo a renvoyé une erreur", Nat.LOG_NORMAL);
					OO_Ok = false;
				}
			}
		}
		else{gest.afficheMessage("\nSystème d'exploitation inconnu", Nat.LOG_NORMAL); OO_Ok = false;}
		
		return OO_Ok ;
	}

    /**
     * Détruit si nécessaire, selon la plateforme, le processus OpenOffice
     * utilisé pour la conversion si celui-ci a été crée par NAT.
     */
    public void killOO()
    {
        if (System.getProperty("os.name").startsWith("Linux"))
        {
            if (!openOfficeRunning) {
                try {
                    Runtime.getRuntime().exec("kill " + pidSoffice);
                } catch (IOException e) {
                    gest.afficheMessage("\nErreur lors " +
                            "de la tentative d'arrêt du processus OpenOffice." +
                            "\n Tentez de terminer ce processus manuellement\n",
                            Nat.LOG_SILENCIEUX);
                }
            }
        }
		
		//Tuer aussi pour windows 
    }
}