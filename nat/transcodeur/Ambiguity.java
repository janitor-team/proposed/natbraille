/*
 * Trace assistant
 * Copyright (C) 2008 Bruno Mascret
 * Contact: bmascret@free.fr
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package nat.transcodeur;

import java.util.ArrayList;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Représente une ambiguité
 * @author bruno
 *
 */
public class Ambiguity
{
	/** chaine de description de l'ambiguïté */
	private String description ="";
	/** liste des solutions possibles en braille*/
	private ArrayList<String> solutions = new ArrayList<String>();
	/** liste des descriptions des solutions */
	private ArrayList<String> descSolution = new ArrayList<String>();
	
	/**
     * @param ambXML le noeud ambiguité d'un fichier transcrit
     */
    public Ambiguity(Node ambXML)
    {
	    NodeList fils = ambXML.getChildNodes();
	    for(int i = 0; i< fils.getLength(); i++)
	    {
	    	Node f = fils.item(i);
	    	if (f.getNodeName().equals("description"))
	    	{
	    		description = f.getTextContent();
	    	}
	    	else if(f.getNodeName().equals("proposition"))
	    	{
	    		Node p = f.getChildNodes().item(0);
	    		Node d = f.getChildNodes().item(1);
	    		if(p.getNodeName().equals("desc"))
	    		{
	    			descSolution.add(p.getTextContent());
	    			solutions.add(d.getTextContent());
	    		}
	    		else
	    		{
	    			descSolution.add(d.getTextContent());
	    			solutions.add(p.getTextContent());
	    		}
	    	}
	    }
    }
    
    /**
     * Pour les tests
     * Affiche une ambiguité dans la console
     */
    public void afficheAmbiguity()
    {
    	System.out.println("Ambiguité:" + description + "\nPropositions:");
    	for(int i=0; i<solutions.size();i++)
    	{
    		System.out.println(" - "+descSolution.get(i));
    		System.out.println(" * "+solutions.get(i));
    	}
    }

}
